package com.wenestim.ecc.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.master.ECC_MST_FS_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_FS_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_FS_Impl.class);

	public List<HashMap<String,Object>> getFinancialStatementHierarchyList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_FS_Mapper mECC_MST_FS_Mapper = sqlSession.getMapper(ECC_MST_FS_Mapper.class);
			return mECC_MST_FS_Mapper.getFinancialStatementHierarchyList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> checkFSCodeExists(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_FS_Mapper mECC_MST_FS_Mapper = sqlSession.getMapper(ECC_MST_FS_Mapper.class);
			return mECC_MST_FS_Mapper.checkFSCodeExists(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getFSParentCodeList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_FS_Mapper mECC_MST_FS_Mapper = sqlSession.getMapper(ECC_MST_FS_Mapper.class);
			return mECC_MST_FS_Mapper.getFSParentCodeList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getFSChildCodeList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_FS_Mapper mECC_MST_FS_Mapper = sqlSession.getMapper(ECC_MST_FS_Mapper.class);
			return mECC_MST_FS_Mapper.getFSChildCodeList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> existsFixName(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_FS_Mapper mECC_MST_FS_Mapper = sqlSession.getMapper(ECC_MST_FS_Mapper.class);
			return mECC_MST_FS_Mapper.existsFixName(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public String createFS(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_FS_Mapper mECC_MST_FS_Mapper = sqlSession.getMapper(ECC_MST_FS_Mapper.class);
			sqlSession.commit();
			return mECC_MST_FS_Mapper.createFS(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean insertFSMaster(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_FS_Mapper mECC_MST_FS_Mapper = sqlSession.getMapper(ECC_MST_FS_Mapper.class);
			mECC_MST_FS_Mapper.insertFSMaster(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateFSMaster(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_FS_Mapper mECC_MST_FS_Mapper = sqlSession.getMapper(ECC_MST_FS_Mapper.class);
			mECC_MST_FS_Mapper.updateFSMaster(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteFSMaster(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_FS_Mapper mECC_MST_FS_Mapper = sqlSession.getMapper(ECC_MST_FS_Mapper.class);
			mECC_MST_FS_Mapper.deleteFSMaster(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
