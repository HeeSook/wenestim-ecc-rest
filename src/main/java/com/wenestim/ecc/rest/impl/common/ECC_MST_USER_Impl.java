package com.wenestim.ecc.rest.impl.common;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.common.ECC_MST_USER_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_USER_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_USER_Impl.class);

	public List<HashMap<String,Object>> getUserList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_USER_Mapper mECC_MST_USER_Mapper = sqlSession.getMapper(ECC_MST_USER_Mapper.class);
			return mECC_MST_USER_Mapper.getUserList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateUserList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_USER_Mapper mECC_MST_USER_Mapper = sqlSession.getMapper(ECC_MST_USER_Mapper.class);
			mECC_MST_USER_Mapper.updateUserList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updatePwResetList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_USER_Mapper mECC_MST_USER_Mapper = sqlSession.getMapper(ECC_MST_USER_Mapper.class);
			mECC_MST_USER_Mapper.updatePwResetList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateMyInfo(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_USER_Mapper mECC_MST_USER_Mapper = sqlSession.getMapper(ECC_MST_USER_Mapper.class);
			mECC_MST_USER_Mapper.updateMyInfo(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteUserList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_USER_Mapper mECC_MST_USER_Mapper = sqlSession.getMapper(ECC_MST_USER_Mapper.class);
			mECC_MST_USER_Mapper.deleteUserList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}


