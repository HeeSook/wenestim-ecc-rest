package com.wenestim.ecc.rest.impl.common;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.common.ECC_MST_USER_PLAID_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_USER_PLAID_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_USER_PLAID_Impl.class);

	public List<HashMap<String,Object>> getUserAccToken(HashMap<String,Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_USER_PLAID_Mapper mECC_MST_USER_PLAID_Mapper = sqlSession.getMapper(ECC_MST_USER_PLAID_Mapper.class);
			return mECC_MST_USER_PLAID_Mapper.getUserAccToken(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateUserAccToken(HashMap<String,Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_USER_PLAID_Mapper mECC_MST_USER_PLAID_Mapper = sqlSession.getMapper(ECC_MST_USER_PLAID_Mapper.class);
			mECC_MST_USER_PLAID_Mapper.updateUserAccToken(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

}
