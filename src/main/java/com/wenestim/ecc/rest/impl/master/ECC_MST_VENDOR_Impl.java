package com.wenestim.ecc.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.master.ECC_MST_VENDOR_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_VENDOR_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_VENDOR_Impl.class);

	public List<HashMap<String,Object>> getVendorList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_VENDOR_Mapper mECC_MST_VENDOR_Mapper = sqlSession.getMapper(ECC_MST_VENDOR_Mapper.class);
			return mECC_MST_VENDOR_Mapper.getVendorList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateVendorList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_VENDOR_Mapper mECC_MST_VENDOR_Mapper = sqlSession.getMapper(ECC_MST_VENDOR_Mapper.class);
			mECC_MST_VENDOR_Mapper.updateVendorList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteVendorList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_VENDOR_Mapper mECC_MST_VENDOR_Mapper = sqlSession.getMapper(ECC_MST_VENDOR_Mapper.class);
			mECC_MST_VENDOR_Mapper.deleteVendorList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public List<HashMap<String,Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_VENDOR_Mapper mECC_MST_VENDOR_Mapper = sqlSession.getMapper(ECC_MST_VENDOR_Mapper.class);
			return mECC_MST_VENDOR_Mapper.getExcelUploadCheckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
