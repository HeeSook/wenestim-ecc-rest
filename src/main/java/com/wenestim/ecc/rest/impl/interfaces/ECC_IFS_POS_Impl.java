package com.wenestim.ecc.rest.impl.interfaces;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.interfaces.ECC_IFS_POS_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_IFS_POS_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_IFS_POS_Impl.class);

	public List<HashMap<String,Object>> getPosIdList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_IFS_POS_Mapper mECC_IFS_POS_Mapper = sqlSession.getMapper(ECC_IFS_POS_Mapper.class);
			return mECC_IFS_POS_Mapper.getPosIdList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getPosInterfaceList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_IFS_POS_Mapper mECC_IFS_POS_Mapper = sqlSession.getMapper(ECC_IFS_POS_Mapper.class);
			return mECC_IFS_POS_Mapper.getPosInterfaceList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updatePosInterfaceList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_IFS_POS_Mapper mECC_IFS_POS_Mapper = sqlSession.getMapper(ECC_IFS_POS_Mapper.class);
			mECC_IFS_POS_Mapper.updatePosInterfaceList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean postPOSData(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_IFS_POS_Mapper mECC_IFS_POS_Mapper = sqlSession.getMapper(ECC_IFS_POS_Mapper.class);
			mECC_IFS_POS_Mapper.postPOSData(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}


