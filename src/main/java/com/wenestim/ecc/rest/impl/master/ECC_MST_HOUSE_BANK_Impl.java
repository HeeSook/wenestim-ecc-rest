package com.wenestim.ecc.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.master.ECC_MST_HOUSE_BANK_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_HOUSE_BANK_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_HOUSE_BANK_Impl.class);

	public List<HashMap<String,Object>> getBankHeaderList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_HOUSE_BANK_Mapper mECC_MST_HOUSE_BANK_Mapper = sqlSession.getMapper(ECC_MST_HOUSE_BANK_Mapper.class);
			return mECC_MST_HOUSE_BANK_Mapper.getBankHeaderList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getBankDetailList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_HOUSE_BANK_Mapper mECC_MST_HOUSE_BANK_Mapper = sqlSession.getMapper(ECC_MST_HOUSE_BANK_Mapper.class);
			return mECC_MST_HOUSE_BANK_Mapper.getBankDetailList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getBankAccIdList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_HOUSE_BANK_Mapper mECC_MST_HOUSE_BANK_Mapper = sqlSession.getMapper(ECC_MST_HOUSE_BANK_Mapper.class);
			return mECC_MST_HOUSE_BANK_Mapper.getBankAccIdList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateBankHeaderList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_HOUSE_BANK_Mapper mECC_MST_HOUSE_BANK_Mapper = sqlSession.getMapper(ECC_MST_HOUSE_BANK_Mapper.class);
			mECC_MST_HOUSE_BANK_Mapper.updateBankHeaderList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateBankDetailList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_HOUSE_BANK_Mapper mECC_MST_HOUSE_BANK_Mapper = sqlSession.getMapper(ECC_MST_HOUSE_BANK_Mapper.class);
			mECC_MST_HOUSE_BANK_Mapper.updateBankDetailList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteBankHeaderList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_HOUSE_BANK_Mapper mECC_MST_HOUSE_BANK_Mapper = sqlSession.getMapper(ECC_MST_HOUSE_BANK_Mapper.class);
			mECC_MST_HOUSE_BANK_Mapper.deleteBankHeaderList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteBankDetailList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_HOUSE_BANK_Mapper mECC_MST_HOUSE_BANK_Mapper = sqlSession.getMapper(ECC_MST_HOUSE_BANK_Mapper.class);
			mECC_MST_HOUSE_BANK_Mapper.deleteBankDetailList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
