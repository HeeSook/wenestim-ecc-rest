package com.wenestim.ecc.rest.impl.approvalline;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.approvalline.ECC_MST_APRL_LINE_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_APRL_LINE_Impl
{
	final static Logger logger = LogManager.getLogger(ECC_MST_APRL_LINE_Impl.class);

	public List<HashMap<String, Object>> getApprovalTypeHeaderList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_APRL_LINE_Mapper mECC_MST_APRL_LINE_Mapper = sqlSession.getMapper(ECC_MST_APRL_LINE_Mapper.class);
			return mECC_MST_APRL_LINE_Mapper.getApprovalTypeHeaderList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String, Object>> getApprovalTypeDetailList (HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_APRL_LINE_Mapper mECC_MST_APRL_LINE_Mapper = sqlSession.getMapper(ECC_MST_APRL_LINE_Mapper.class);
			return mECC_MST_APRL_LINE_Mapper.getApprovalTypeDetailList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}

	}

	public String getApprovalLineCode(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			ECC_MST_APRL_LINE_Mapper mECC_MST_APRL_LINE_Mapper = sqlSession.getMapper(ECC_MST_APRL_LINE_Mapper.class);
			return mECC_MST_APRL_LINE_Mapper.getApprovalLineCode(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean insertApprovalTypeHeaderList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_APRL_LINE_Mapper mECC_MST_APRL_LINE_Mapper = sqlSession.getMapper(ECC_MST_APRL_LINE_Mapper.class);
			mECC_MST_APRL_LINE_Mapper.insertApprovalTypeHeaderList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateApprovalTypeHeaderList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_APRL_LINE_Mapper mECC_MST_APRL_LINE_Mapper = sqlSession.getMapper(ECC_MST_APRL_LINE_Mapper.class);
			mECC_MST_APRL_LINE_Mapper.updateApprovalTypeHeaderList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateApprovalTypeDetailList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_APRL_LINE_Mapper mECC_MST_APRL_LINE_Mapper = sqlSession.getMapper(ECC_MST_APRL_LINE_Mapper.class);
			mECC_MST_APRL_LINE_Mapper.updateApprovalTypeDetailList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteApprovalTypeDetailList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_APRL_LINE_Mapper mECC_MST_APRL_LINE_Mapper = sqlSession.getMapper(ECC_MST_APRL_LINE_Mapper.class);
			mECC_MST_APRL_LINE_Mapper.deleteApprovalTypeDetailList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

}