package com.wenestim.ecc.rest.impl.fs;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.fs.ECC_DYN_FS_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_FS_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_FS_Impl.class);

	public List<HashMap<String,Object>> getFinancialStatementList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_FS_Mapper mECC_DYN_FS_Mapper = sqlSession.getMapper(ECC_DYN_FS_Mapper.class);
			return mECC_DYN_FS_Mapper.getFinancialStatementList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getConsolidationList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_FS_Mapper mECC_DYN_FS_Mapper = sqlSession.getMapper(ECC_DYN_FS_Mapper.class);
			return mECC_DYN_FS_Mapper.getConsolidationList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getDashboardDataList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_FS_Mapper mECC_DYN_FS_Mapper = sqlSession.getMapper(ECC_DYN_FS_Mapper.class);
			return mECC_DYN_FS_Mapper.getDashboardDataList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
