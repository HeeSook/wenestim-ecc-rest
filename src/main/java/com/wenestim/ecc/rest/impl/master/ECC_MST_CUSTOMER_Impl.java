package com.wenestim.ecc.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.master.ECC_MST_CUSTOMER_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_CUSTOMER_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_CUSTOMER_Impl.class);

	public List<HashMap<String,Object>> getCustomerList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_CUSTOMER_Mapper mECC_MST_CUSTOMER_Mapper = sqlSession.getMapper(ECC_MST_CUSTOMER_Mapper.class);
			return mECC_MST_CUSTOMER_Mapper.getCustomerList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateCustomerList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_CUSTOMER_Mapper mECC_MST_CUSTOMER_Mapper = sqlSession.getMapper(ECC_MST_CUSTOMER_Mapper.class);
			mECC_MST_CUSTOMER_Mapper.updateCustomerList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteCustomerList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_CUSTOMER_Mapper mECC_MST_CUSTOMER_Mapper = sqlSession.getMapper(ECC_MST_CUSTOMER_Mapper.class);
			mECC_MST_CUSTOMER_Mapper.deleteCustomerList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public List<HashMap<String,Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_CUSTOMER_Mapper mECC_MST_CUSTOMER_Mapper = sqlSession.getMapper(ECC_MST_CUSTOMER_Mapper.class);
			return mECC_MST_CUSTOMER_Mapper.getExcelUploadCheckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
