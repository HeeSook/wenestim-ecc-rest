package com.wenestim.ecc.rest.impl.documents;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.wenestim.ecc.rest.mapper.documents.ECC_DYN_DOC_FILE_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;
import com.wenestim.ecc.rest.util.StringUtil;

public class ECC_DYN_DOC_FILE_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_DOC_FILE_Impl.class);

	public List<HashMap<String,Object>> getFileList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_FILE_Mapper mECC_DYN_DOC_FILE_Mapper = sqlSession.getMapper(ECC_DYN_DOC_FILE_Mapper.class);
			return mECC_DYN_DOC_FILE_Mapper.getFileList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateFiles(HashMap<String, Object> dataMap)
	{
		Gson gson = new Gson();

		String    attachItem = (String)dataMap.get("attachItem") ;
		ArrayList attachList = StringUtil.getJsonToHashMapList(attachItem);

		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
//			logger.info("Array Size: " + attachList.size());
			for (int i = 0; i < attachList.size(); i++) {
	            try {
	            	String temp = gson.toJson(attachList.get(i));
	            	JsonElement jelement = new JsonParser().parse(temp);
	                JsonObject  jobject = jelement.getAsJsonObject();

	                String name      = jobject.get("FILE_NAME") != null ? jobject.get("FILE_NAME").getAsString() : "";
	                String raw       = jobject.get("FILE_DATA") != null ? jobject.get("FILE_DATA").getAsString() : "";
	                String filepath  = jobject.get("FILE_PATH") != null ? jobject.get("FILE_PATH").getAsString() : "";

	                if(!raw.isEmpty()) {
		                File path = new File("c://ecc/" + filepath + File.separator + dataMap.get("DOC_NO"));

		            	logger.info(name);

		            	path.mkdirs();

		            	String fileName = path.getCanonicalPath() + File.separator + name;

		            	FileOutputStream fos = new FileOutputStream(fileName);
		            	fos.write(Base64.getDecoder().decode(raw));
		            	fos.close();
	                }

	            } catch (Exception e) {
	            	logger.error("Exception:" + e.getMessage());
	                e.printStackTrace();
	            }
	        }

			ECC_DYN_DOC_FILE_Mapper mECC_DYN_DOC_FILE_Mapper = sqlSession.getMapper(ECC_DYN_DOC_FILE_Mapper.class);
			mECC_DYN_DOC_FILE_Mapper.updateFiles(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}