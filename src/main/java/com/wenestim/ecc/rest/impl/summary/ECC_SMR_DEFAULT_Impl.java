package com.wenestim.ecc.rest.impl.summary;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.summary.ECC_SMR_DEFAULT_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_SMR_DEFAULT_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_SMR_DEFAULT_Impl.class);

	public List<HashMap<String,Object>> getDocStatusList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_SMR_DEFAULT_Mapper mECC_SMR_DEFAULT_Mapper = sqlSession.getMapper(ECC_SMR_DEFAULT_Mapper.class);
			return mECC_SMR_DEFAULT_Mapper.getDocStatusList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public HashMap<String,Object> getApprovalWaitList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_SMR_DEFAULT_Mapper mECC_SMR_DEFAULT_Mapper = sqlSession.getMapper(ECC_SMR_DEFAULT_Mapper.class);
			return mECC_SMR_DEFAULT_Mapper.getApprovalWaitList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getProfitLossList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_SMR_DEFAULT_Mapper mECC_SMR_DEFAULT_Mapper = sqlSession.getMapper(ECC_SMR_DEFAULT_Mapper.class);
			return mECC_SMR_DEFAULT_Mapper.getProfitLossList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getBottleNeckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_SMR_DEFAULT_Mapper mECC_SMR_DEFAULT_Mapper = sqlSession.getMapper(ECC_SMR_DEFAULT_Mapper.class);
			return mECC_SMR_DEFAULT_Mapper.getBottleNeckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
