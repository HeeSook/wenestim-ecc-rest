package com.wenestim.ecc.rest.impl.payment;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.payment.ECC_DYN_PAYMENT_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_PAYMENT_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_PAYMENT_Impl.class);

	public List<HashMap<String,Object>> getOpenItemList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_PAYMENT_Mapper mECC_DYN_PAYMENT_Mapper = sqlSession.getMapper(ECC_DYN_PAYMENT_Mapper.class);
			return mECC_DYN_PAYMENT_Mapper.getOpenItemList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getSimulationList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_PAYMENT_Mapper mECC_DYN_PAYMENT_Mapper = sqlSession.getMapper(ECC_DYN_PAYMENT_Mapper.class);
			return mECC_DYN_PAYMENT_Mapper.getSimulationList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean postPaymentList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_PAYMENT_Mapper mECC_DYN_PAYMENT_Mapper = sqlSession.getMapper(ECC_DYN_PAYMENT_Mapper.class);
			mECC_DYN_PAYMENT_Mapper.postPaymentList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean postInterPaymentList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_PAYMENT_Mapper mECC_DYN_PAYMENT_Mapper = sqlSession.getMapper(ECC_DYN_PAYMENT_Mapper.class);
			mECC_DYN_PAYMENT_Mapper.postInterPaymentList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public List<HashMap<String,Object>> getPostingResultList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_PAYMENT_Mapper mECC_DYN_PAYMENT_Mapper = sqlSession.getMapper(ECC_DYN_PAYMENT_Mapper.class);
			return mECC_DYN_PAYMENT_Mapper.getPostingResultList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getPaymentList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_PAYMENT_Mapper mECC_DYN_PAYMENT_Mapper = sqlSession.getMapper(ECC_DYN_PAYMENT_Mapper.class);
			return mECC_DYN_PAYMENT_Mapper.getPaymentList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}


	public List<HashMap<String,Object>> getIsOpenItemList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_PAYMENT_Mapper mECC_DYN_PAYMENT_Mapper = sqlSession.getMapper(ECC_DYN_PAYMENT_Mapper.class);
			return mECC_DYN_PAYMENT_Mapper.getIsOpenItemList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
