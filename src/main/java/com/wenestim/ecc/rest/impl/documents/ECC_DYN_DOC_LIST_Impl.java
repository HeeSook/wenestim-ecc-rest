package com.wenestim.ecc.rest.impl.documents;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.documents.ECC_DYN_DOC_LIST_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_DOC_LIST_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_DOC_LIST_Impl.class);

	public List<HashMap<String,Object>> getDefaultDocList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			return mECC_DYN_DOC_LIST_Mapper.getDefaultDocList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getInterTransactionDocList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			return mECC_DYN_DOC_LIST_Mapper.getInterTransactionDocList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getApproverDocList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			return mECC_DYN_DOC_LIST_Mapper.getApproverDocList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getDelegateDocList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			return mECC_DYN_DOC_LIST_Mapper.getDelegateDocList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getDocReverseNewNoList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			return mECC_DYN_DOC_LIST_Mapper.getDocReverseNewNoList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getInterTrItemList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			return mECC_DYN_DOC_LIST_Mapper.getInterTrItemList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getOpenItemFlag(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			return mECC_DYN_DOC_LIST_Mapper.getOpenItemFlag(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateDocApprovalList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			mECC_DYN_DOC_LIST_Mapper.updateDocApprovalList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateDocAdditionalList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			mECC_DYN_DOC_LIST_Mapper.updateDocAdditionalList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateDocReverseList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			mECC_DYN_DOC_LIST_Mapper.updateDocReverseList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateDocResetList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			mECC_DYN_DOC_LIST_Mapper.updateDocResetList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateDocMultiReqList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_LIST_Mapper mECC_DYN_DOC_LIST_Mapper = sqlSession.getMapper(ECC_DYN_DOC_LIST_Mapper.class);
			mECC_DYN_DOC_LIST_Mapper.updateDocMultiReqList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
