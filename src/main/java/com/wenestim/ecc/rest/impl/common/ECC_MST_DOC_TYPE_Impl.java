package com.wenestim.ecc.rest.impl.common;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.common.ECC_MST_DOC_TYPE_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_DOC_TYPE_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_DOC_TYPE_Impl.class);

	public List<HashMap<String,Object>> getDocTypeList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_DOC_TYPE_Mapper mECC_MST_DOC_TYPE_Mapper = sqlSession.getMapper(ECC_MST_DOC_TYPE_Mapper.class);
			return mECC_MST_DOC_TYPE_Mapper.getDocTypeList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}


