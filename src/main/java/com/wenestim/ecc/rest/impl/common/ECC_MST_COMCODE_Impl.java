package com.wenestim.ecc.rest.impl.common;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.common.ECC_MST_COMCODE_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_COMCODE_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_COMCODE_Impl.class);

	public List<HashMap<String,Object>> getComCodeList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_COMCODE_Mapper mECC_MST_COMCODE_Mapper = sqlSession.getMapper(ECC_MST_COMCODE_Mapper.class);
			return mECC_MST_COMCODE_Mapper.getComCodeList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getComCodeSearchList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_COMCODE_Mapper mECC_MST_COMCODE_Mapper = sqlSession.getMapper(ECC_MST_COMCODE_Mapper.class);
			return mECC_MST_COMCODE_Mapper.getComCodeSearchList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateComCodeList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_COMCODE_Mapper mECC_MST_COMCODE_Mapper = sqlSession.getMapper(ECC_MST_COMCODE_Mapper.class);
			mECC_MST_COMCODE_Mapper.updateComCodeList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
