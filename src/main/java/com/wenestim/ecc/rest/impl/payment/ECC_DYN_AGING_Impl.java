package com.wenestim.ecc.rest.impl.payment;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.payment.ECC_DYN_AGING_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_AGING_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_AGING_Impl.class);

	public List<HashMap<String,Object>> getAgingSummaryList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_AGING_Mapper mECC_DYN_AGING_Mapper = sqlSession.getMapper(ECC_DYN_AGING_Mapper.class);
			return mECC_DYN_AGING_Mapper.getAgingSummaryList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}


	public List<HashMap<String,Object>> getAgingDetailList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_AGING_Mapper mECC_DYN_AGING_Mapper = sqlSession.getMapper(ECC_DYN_AGING_Mapper.class);
			return mECC_DYN_AGING_Mapper.getAgingDetailList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
