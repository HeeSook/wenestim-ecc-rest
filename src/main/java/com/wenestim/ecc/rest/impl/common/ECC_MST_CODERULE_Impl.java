package com.wenestim.ecc.rest.impl.common;

import java.util.HashMap;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.common.ECC_MST_CODERULE_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_CODERULE_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_CODERULE_Impl.class);

	public String getMasterCode(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_CODERULE_Mapper mECC_MST_CODERULE_Mapper = sqlSession.getMapper(ECC_MST_CODERULE_Mapper.class);
			return mECC_MST_CODERULE_Mapper.getMasterCode(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public String getDocNo(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("CATEGORY2"   ) String CATEGORY2   ,
			@Param("CODE"        ) String CODE        ,
			@Param("POST_YEAR"   ) Integer POST_YEAR
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			ECC_MST_CODERULE_Mapper mECC_MST_CODERULE_Mapper = sqlSession.getMapper(ECC_MST_CODERULE_Mapper.class);
			return mECC_MST_CODERULE_Mapper.getDocNo(CLIENT_ID, COMPANY_CODE, CATEGORY2, CODE, POST_YEAR);
		}
		finally
		{
			sqlSession.close();
		}
	}
}


