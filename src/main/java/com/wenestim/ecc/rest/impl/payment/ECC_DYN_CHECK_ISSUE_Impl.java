package com.wenestim.ecc.rest.impl.payment;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.payment.ECC_DYN_CHECK_ISSUE_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_CHECK_ISSUE_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_CHECK_ISSUE_Impl.class);

	public List<HashMap<String,Object>> getCheckIssueList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_CHECK_ISSUE_Mapper mECC_DYN_CHECK_ISSUE_Mapper = sqlSession.getMapper(ECC_DYN_CHECK_ISSUE_Mapper.class);
			return mECC_DYN_CHECK_ISSUE_Mapper.getCheckIssueList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getCheckIssuedList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_CHECK_ISSUE_Mapper mECC_DYN_CHECK_ISSUE_Mapper = sqlSession.getMapper(ECC_DYN_CHECK_ISSUE_Mapper.class);
			return mECC_DYN_CHECK_ISSUE_Mapper.getCheckIssuedList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getCheckPrintHeadList (HashMap<String, Object> paramMap) {

		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_CHECK_ISSUE_Mapper mECC_DYN_CHECK_ISSUE_Mapper = sqlSession.getMapper(ECC_DYN_CHECK_ISSUE_Mapper.class);
			return mECC_DYN_CHECK_ISSUE_Mapper.getCheckPrintHeadList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}

	}

	public List<HashMap<String,Object>> getCheckPrintItemList (HashMap<String, Object> paramMap) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_CHECK_ISSUE_Mapper mECC_DYN_CHECK_ISSUE_Mapper = sqlSession.getMapper(ECC_DYN_CHECK_ISSUE_Mapper.class);
			return mECC_DYN_CHECK_ISSUE_Mapper.getCheckPrintItemList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateCheckAssignList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_CHECK_ISSUE_Mapper mECC_DYN_CHECK_ISSUE_Mapper = sqlSession.getMapper(ECC_DYN_CHECK_ISSUE_Mapper.class);
			mECC_DYN_CHECK_ISSUE_Mapper.updateCheckAssignList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateCheckStatusList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_CHECK_ISSUE_Mapper mECC_DYN_CHECK_ISSUE_Mapper = sqlSession.getMapper(ECC_DYN_CHECK_ISSUE_Mapper.class);
			mECC_DYN_CHECK_ISSUE_Mapper.updateCheckStatusList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
