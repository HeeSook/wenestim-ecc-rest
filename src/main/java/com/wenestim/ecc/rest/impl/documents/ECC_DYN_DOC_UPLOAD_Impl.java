package com.wenestim.ecc.rest.impl.documents;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.documents.ECC_DYN_DOC_UPLOAD_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_DOC_UPLOAD_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_DOC_UPLOAD_Impl.class);

	public List<HashMap<String,Object>> getUploadList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_UPLOAD_Mapper mapper = sqlSession.getMapper(ECC_DYN_DOC_UPLOAD_Mapper.class);
			return mapper.getUploadList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateUploadList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_UPLOAD_Mapper mapper = sqlSession.getMapper(ECC_DYN_DOC_UPLOAD_Mapper.class);
			mapper.updateUploadList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}