package com.wenestim.ecc.rest.impl.payment;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.payment.ECC_DYN_CLEARING_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_CLEARING_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_CLEARING_Impl.class);

	public List<HashMap<String,Object>> getOpenItemList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_CLEARING_Mapper mECC_DYN_CLEARING_Mapper = sqlSession.getMapper(ECC_DYN_CLEARING_Mapper.class);
			return mECC_DYN_CLEARING_Mapper.getOpenItemList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean postClearingList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_CLEARING_Mapper mECC_DYN_CLEARING_Mapper = sqlSession.getMapper(ECC_DYN_CLEARING_Mapper.class);
			mECC_DYN_CLEARING_Mapper.postClearingList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
