package com.wenestim.ecc.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.master.ECC_MST_EX_RATE_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_EX_RATE_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_EX_RATE_Impl.class);

	public List<HashMap<String,Object>> getExRateList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_EX_RATE_Mapper mECC_MST_EX_RATE_Mapper = sqlSession.getMapper(ECC_MST_EX_RATE_Mapper.class);
			return mECC_MST_EX_RATE_Mapper.getExRateList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}


	public Boolean updateExRateList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_EX_RATE_Mapper mECC_MST_EX_RATE_Mapper = sqlSession.getMapper(ECC_MST_EX_RATE_Mapper.class);
			mECC_MST_EX_RATE_Mapper.updateExRateList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteExRateList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_EX_RATE_Mapper mECC_MST_EX_RATE_Mapper = sqlSession.getMapper(ECC_MST_EX_RATE_Mapper.class);
			mECC_MST_EX_RATE_Mapper.deleteExRateList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
