package com.wenestim.ecc.rest.impl.common;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.common.ECC_MST_DEPT_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_DEPT_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_DEPT_Impl.class);

	public List<HashMap<String,Object>> getDeptList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_DEPT_Mapper MECC_MST_DEPT_Mapper = sqlSession.getMapper(ECC_MST_DEPT_Mapper.class);
			return MECC_MST_DEPT_Mapper.getDeptList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getDeptListHierarchyList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_DEPT_Mapper MECC_MST_DEPT_Mapper = sqlSession.getMapper(ECC_MST_DEPT_Mapper.class);
			return MECC_MST_DEPT_Mapper.getDeptListHierarchyList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> checkDeptCodeExists(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_DEPT_Mapper MECC_MST_DEPT_Mapper = sqlSession.getMapper(ECC_MST_DEPT_Mapper.class);
			return MECC_MST_DEPT_Mapper.checkDeptCodeExists(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean insertDept(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_DEPT_Mapper MECC_MST_DEPT_Mapper = sqlSession.getMapper(ECC_MST_DEPT_Mapper.class);
			MECC_MST_DEPT_Mapper.insertDept(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteDept(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_DEPT_Mapper MECC_MST_DEPT_Mapper = sqlSession.getMapper(ECC_MST_DEPT_Mapper.class);
			MECC_MST_DEPT_Mapper.deleteDept(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateDept(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_DEPT_Mapper MECC_MST_DEPT_Mapper = sqlSession.getMapper(ECC_MST_DEPT_Mapper.class);
			MECC_MST_DEPT_Mapper.updateDept(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}


