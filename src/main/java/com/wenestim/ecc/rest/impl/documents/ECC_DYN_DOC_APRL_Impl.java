package com.wenestim.ecc.rest.impl.documents;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.documents.ECC_DYN_DOC_APRL_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_DOC_APRL_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_DOC_APRL_Impl.class);

	public List<HashMap<String,Object>> getAprlList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_APRL_Mapper mECC_DYN_DOC_APRL_Mapper = sqlSession.getMapper(ECC_DYN_DOC_APRL_Mapper.class);
			return mECC_DYN_DOC_APRL_Mapper.getAprlList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getFavoriteAprlList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_APRL_Mapper mECC_DYN_DOC_APRL_Mapper = sqlSession.getMapper(ECC_DYN_DOC_APRL_Mapper.class);
			return mECC_DYN_DOC_APRL_Mapper.getFavoriteAprlList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getDefaultAprlList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_APRL_Mapper mECC_DYN_DOC_APRL_Mapper = sqlSession.getMapper(ECC_DYN_DOC_APRL_Mapper.class);
			mECC_DYN_DOC_APRL_Mapper.callDefaultAprlList(paramMap);
			return mECC_DYN_DOC_APRL_Mapper.getDefaultAprlList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateParkAprl(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_APRL_Mapper mECC_DYN_DOC_APRL_Mapper = sqlSession.getMapper(ECC_DYN_DOC_APRL_Mapper.class);
			mECC_DYN_DOC_APRL_Mapper.updateParkAprl(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateFavoriteAprl(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_APRL_Mapper mECC_DYN_DOC_APRL_Mapper = sqlSession.getMapper(ECC_DYN_DOC_APRL_Mapper.class);
			mECC_DYN_DOC_APRL_Mapper.updateFavoriteAprl(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}