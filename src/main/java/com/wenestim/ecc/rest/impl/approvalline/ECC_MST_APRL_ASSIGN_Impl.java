package com.wenestim.ecc.rest.impl.approvalline;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.approvalline.ECC_MST_APRL_ASSIGN_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_APRL_ASSIGN_Impl
{
	final static Logger logger   = LogManager.getLogger(ECC_MST_APRL_ASSIGN_Impl.class);

	public List<HashMap<String,Object>> getAprlLineList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_APRL_ASSIGN_Mapper mECC_MST_APRL_ASSIGN_Mapper = sqlSession.getMapper(ECC_MST_APRL_ASSIGN_Mapper.class);
			return mECC_MST_APRL_ASSIGN_Mapper.getAprlLineList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getAprlAssingList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_APRL_ASSIGN_Mapper mECC_MST_APRL_ASSIGN_Mapper = sqlSession.getMapper(ECC_MST_APRL_ASSIGN_Mapper.class);
			return mECC_MST_APRL_ASSIGN_Mapper.getAprlAssingList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateAprlAssingList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_APRL_ASSIGN_Mapper mECC_MST_APRL_ASSIGN_Mapper = sqlSession.getMapper(ECC_MST_APRL_ASSIGN_Mapper.class);
			mECC_MST_APRL_ASSIGN_Mapper.updateAprlAssingList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
