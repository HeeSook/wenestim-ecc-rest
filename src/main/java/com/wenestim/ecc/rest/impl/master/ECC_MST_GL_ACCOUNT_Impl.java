package com.wenestim.ecc.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.master.ECC_MST_GL_ACCOUNT_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_GL_ACCOUNT_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_GL_ACCOUNT_Impl.class);

	public List<HashMap<String,Object>> getCoaGlAccountList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_GL_ACCOUNT_Mapper mECC_MST_GL_ACCOUNT_Mapper = sqlSession.getMapper(ECC_MST_GL_ACCOUNT_Mapper.class);
			return mECC_MST_GL_ACCOUNT_Mapper.getCoaGlAccountList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getCompanyGlAccountList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_GL_ACCOUNT_Mapper mECC_MST_GL_ACCOUNT_Mapper = sqlSession.getMapper(ECC_MST_GL_ACCOUNT_Mapper.class);
			return mECC_MST_GL_ACCOUNT_Mapper.getCompanyGlAccountList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateCoaGlAccountList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_GL_ACCOUNT_Mapper mECC_MST_GL_ACCOUNT_Mapper = sqlSession.getMapper(ECC_MST_GL_ACCOUNT_Mapper.class);
			mECC_MST_GL_ACCOUNT_Mapper.updateCoaGlAccountList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean createCompanyGlAccountList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_GL_ACCOUNT_Mapper mECC_MST_GL_ACCOUNT_Mapper = sqlSession.getMapper(ECC_MST_GL_ACCOUNT_Mapper.class);
			mECC_MST_GL_ACCOUNT_Mapper.createCompanyGlAccountList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateCompanyGlAccountList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_GL_ACCOUNT_Mapper mECC_MST_GL_ACCOUNT_Mapper = sqlSession.getMapper(ECC_MST_GL_ACCOUNT_Mapper.class);
			mECC_MST_GL_ACCOUNT_Mapper.updateCompanyGlAccountList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteCoaGlAccountList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_GL_ACCOUNT_Mapper mECC_MST_GL_ACCOUNT_Mapper = sqlSession.getMapper(ECC_MST_GL_ACCOUNT_Mapper.class);
			mECC_MST_GL_ACCOUNT_Mapper.deleteCoaGlAccountList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteCompanyGlAccountList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_GL_ACCOUNT_Mapper mECC_MST_GL_ACCOUNT_Mapper = sqlSession.getMapper(ECC_MST_GL_ACCOUNT_Mapper.class);
			mECC_MST_GL_ACCOUNT_Mapper.deleteCompanyGlAccountList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public List<HashMap<String,Object>> getCoaExcelUploadCheckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_GL_ACCOUNT_Mapper mECC_MST_GL_ACCOUNT_Mapper = sqlSession.getMapper(ECC_MST_GL_ACCOUNT_Mapper.class);
			return mECC_MST_GL_ACCOUNT_Mapper.getCoaExcelUploadCheckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
