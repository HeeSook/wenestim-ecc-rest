package com.wenestim.ecc.rest.impl.bank;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.bank.ECC_DYN_BANK_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_BANK_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_BANK_Impl.class);

	public List<HashMap<String,Object>> getBankStatementList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_BANK_Mapper mECC_DYN_BANK_Mapper = sqlSession.getMapper(ECC_DYN_BANK_Mapper.class);
			return mECC_DYN_BANK_Mapper.getBankStatementList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getClearMappingList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_BANK_Mapper mECC_DYN_BANK_Mapper = sqlSession.getMapper(ECC_DYN_BANK_Mapper.class);
			return mECC_DYN_BANK_Mapper.getClearMappingList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getVendorCustItemList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_BANK_Mapper mECC_DYN_BANK_Mapper = sqlSession.getMapper(ECC_DYN_BANK_Mapper.class);
			return mECC_DYN_BANK_Mapper.getVendorCustItemList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getOutstandingBalanceList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_BANK_Mapper mECC_DYN_BANK_Mapper = sqlSession.getMapper(ECC_DYN_BANK_Mapper.class);
			return mECC_DYN_BANK_Mapper.getOutstandingBalanceList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getAccountBalanceList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_BANK_Mapper mECC_DYN_BANK_Mapper = sqlSession.getMapper(ECC_DYN_BANK_Mapper.class);
			return mECC_DYN_BANK_Mapper.getAccountBalanceList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
