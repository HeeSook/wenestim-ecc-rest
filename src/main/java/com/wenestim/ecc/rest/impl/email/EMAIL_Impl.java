package com.wenestim.ecc.rest.impl.email;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.email.EMAIL_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class EMAIL_Impl
{
	private final static Logger logger = LogManager.getLogger(EMAIL_Impl.class);

	public List<HashMap<String, Object>> getEmailData(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			EMAIL_Mapper mEMAIL_Mapper = sqlSession.getMapper(EMAIL_Mapper.class);
			return mEMAIL_Mapper.getEmailData(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String, Object>> getUserStatusData(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			EMAIL_Mapper mEMAIL_Mapper = sqlSession.getMapper(EMAIL_Mapper.class);
			return mEMAIL_Mapper.getUserStatusData(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}