package com.wenestim.ecc.rest.impl.common;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.common.ECC_MST_COMPANY_GROUP_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_MST_COMPANY_GROUP_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_COMPANY_GROUP_Impl.class);

	public List<HashMap<String,Object>> getCompanyGroupList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_COMPANY_GROUP_Mapper mECC_MST_COMPANY_GROUP_Mapper = sqlSession.getMapper(ECC_MST_COMPANY_GROUP_Mapper.class);
			return mECC_MST_COMPANY_GROUP_Mapper.getCompanyGroupList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getCompanyGroupSearchList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_COMPANY_GROUP_Mapper mECC_MST_COMPANY_GROUP_Mapper = sqlSession.getMapper(ECC_MST_COMPANY_GROUP_Mapper.class);
			return mECC_MST_COMPANY_GROUP_Mapper.getCompanyGroupSearchList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getCompanyGroupChildSearchList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_COMPANY_GROUP_Mapper mECC_MST_COMPANY_GROUP_Mapper = sqlSession.getMapper(ECC_MST_COMPANY_GROUP_Mapper.class);
			return mECC_MST_COMPANY_GROUP_Mapper.getCompanyGroupChildSearchList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getCGParentCodeList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_COMPANY_GROUP_Mapper mECC_MST_COMPANY_GROUP_Mapper = sqlSession.getMapper(ECC_MST_COMPANY_GROUP_Mapper.class);
			return mECC_MST_COMPANY_GROUP_Mapper.getCGParentCodeList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> checkCGCodeExists(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_MST_COMPANY_GROUP_Mapper mECC_MST_COMPANY_GROUP_Mapper = sqlSession.getMapper(ECC_MST_COMPANY_GROUP_Mapper.class);
			return mECC_MST_COMPANY_GROUP_Mapper.checkCGCodeExists(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean createCompanyGroupList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_COMPANY_GROUP_Mapper mECC_MST_COMPANY_GROUP_Mapper = sqlSession.getMapper(ECC_MST_COMPANY_GROUP_Mapper.class);
			mECC_MST_COMPANY_GROUP_Mapper.createCompanyGroupList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateCompanyGroupList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_COMPANY_GROUP_Mapper mECC_MST_COMPANY_GROUP_Mapper = sqlSession.getMapper(ECC_MST_COMPANY_GROUP_Mapper.class);
			mECC_MST_COMPANY_GROUP_Mapper.updateCompanyGroupList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteCompanyGroupList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_MST_COMPANY_GROUP_Mapper mECC_MST_COMPANY_GROUP_Mapper = sqlSession.getMapper(ECC_MST_COMPANY_GROUP_Mapper.class);
			mECC_MST_COMPANY_GROUP_Mapper.deleteCompanyGroupList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}


