package com.wenestim.ecc.rest.impl.documents;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.documents.ECC_DYN_DOC_PARK_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_DOC_PARK_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_DOC_PARK_Impl.class);

	public String getCheckDocNo(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			return mECC_DYN_DOC_PARK_Mapper.getCheckDocNo(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public String getCheckPeriod(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			return mECC_DYN_DOC_PARK_Mapper.getCheckPeriod(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public String getCheckInvoice(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			return mECC_DYN_DOC_PARK_Mapper.getCheckInvoice(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public HashMap<String,Object> getDocExRate(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			return mECC_DYN_DOC_PARK_Mapper.getDocExRate(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public HashMap getPaymentTerms(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			return mECC_DYN_DOC_PARK_Mapper.getPaymentTerms(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public HashMap<String,Object> getHeaderList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			return mECC_DYN_DOC_PARK_Mapper.getHeaderList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getItemList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			return mECC_DYN_DOC_PARK_Mapper.getItemList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getClearList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			return mECC_DYN_DOC_PARK_Mapper.getClearList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateParkHeader(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			mECC_DYN_DOC_PARK_Mapper.updateParkHeader(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateParkItems(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			mECC_DYN_DOC_PARK_Mapper.updateParkItems(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateDirectPosting(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			mECC_DYN_DOC_PARK_Mapper.updateDirectPosting(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteParkItems(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			ECC_DYN_DOC_PARK_Mapper mECC_DYN_DOC_PARK_Mapper = sqlSession.getMapper(ECC_DYN_DOC_PARK_Mapper.class);
			mECC_DYN_DOC_PARK_Mapper.deleteParkItems(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

}