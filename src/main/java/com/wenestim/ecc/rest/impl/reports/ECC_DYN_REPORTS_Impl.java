package com.wenestim.ecc.rest.impl.reports;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.wenestim.ecc.rest.mapper.reports.ECC_DYN_REPORTS_Mapper;
import com.wenestim.ecc.rest.util.MyBatisUtil;

public class ECC_DYN_REPORTS_Impl
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_REPORTS_Impl.class);

	public List<HashMap<String,Object>> getTrialBalanceList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_REPORTS_Mapper mECC_DYN_REPORTS_Mapper = sqlSession.getMapper(ECC_DYN_REPORTS_Mapper.class);
			return mECC_DYN_REPORTS_Mapper.getTrialBalanceList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getGLItemList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_REPORTS_Mapper mECC_DYN_REPORTS_Mapper = sqlSession.getMapper(ECC_DYN_REPORTS_Mapper.class);
			return mECC_DYN_REPORTS_Mapper.getGLItemList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getSLItemList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_REPORTS_Mapper mECC_DYN_REPORTS_Mapper = sqlSession.getMapper(ECC_DYN_REPORTS_Mapper.class);
			return mECC_DYN_REPORTS_Mapper.getSLItemList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getPLCostConeterList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			ECC_DYN_REPORTS_Mapper mECC_DYN_REPORTS_Mapper = sqlSession.getMapper(ECC_DYN_REPORTS_Mapper.class);
			return mECC_DYN_REPORTS_Mapper.getPLCostConeterList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
