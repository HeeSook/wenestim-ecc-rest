package com.wenestim.ecc.rest.service.master;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.master.ECC_MST_VENDOR_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Master/Vendor")
public class ECC_MST_VENDOR_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_VENDOR_Service.class);

	@POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getVendorList(InputStream inputStream)
    {
    	logger.info("ECC_MST_VENDOR_Service.getVendorList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_VENDOR_Impl iECC_MST_VENDOR_Impl = new ECC_MST_VENDOR_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_VENDOR_Impl.getVendorList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateVendorList(InputStream inputStream)
    {
		logger.info("ECC_MST_VENDOR_Service.updateVendorList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_VENDOR_Impl iECC_MST_VENDOR_Impl = new ECC_MST_VENDOR_Impl();
        	rtnBoolResult = iECC_MST_VENDOR_Impl.updateVendorList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteVendorList(InputStream inputStream)
    {
		logger.info("ECC_MST_VENDOR_Service.deleteVendorList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_VENDOR_Impl iECC_MST_VENDOR_Impl = new ECC_MST_VENDOR_Impl();
        	rtnBoolResult = iECC_MST_VENDOR_Impl.deleteVendorList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/ExcelUpload/CheckData")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getExcelUploadCheckList(InputStream inputStream)
    {
    	logger.info("ECC_MST_VENDOR_Service.getExcelUploadCheckList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

        	ECC_MST_VENDOR_Impl iECC_MST_VENDOR_Impl = new ECC_MST_VENDOR_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_VENDOR_Impl.getExcelUploadCheckList(paramMap);

        	Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/ExcelUpload/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateExcelUploadDataList(InputStream inputStream)
    {
		logger.info("ECC_MST_VENDOR_Service.updateExcelUploadDataList() called");

        return updateVendorList(inputStream);
    }
}
