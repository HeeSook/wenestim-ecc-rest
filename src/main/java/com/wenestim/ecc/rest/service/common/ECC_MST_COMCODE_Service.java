package com.wenestim.ecc.rest.service.common;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.common.ECC_MST_COMCODE_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Common/ComCode")
public class ECC_MST_COMCODE_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_COMCODE_Service.class);

	@POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getComCodeList(InputStream inputStream)
    {
    	logger.info("ECC_MST_COMCODE_Service.getComCodeList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_COMCODE_Impl iECC_MST_COMCODE_Impl = new ECC_MST_COMCODE_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_COMCODE_Impl.getComCodeList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Search/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getComCodeSearchList(InputStream inputStream)
    {
    	logger.info("ECC_MST_COMCODE_Service.getComCodeSearchList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_COMCODE_Impl iECC_MST_COMCODE_Impl = new ECC_MST_COMCODE_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_COMCODE_Impl.getComCodeSearchList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateComCodeList(InputStream inputStream)
    {
		logger.info("ECC_MST_COMCODE_Service.updateComCodeList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_COMCODE_Impl iECC_MST_COMCODE_Impl = new ECC_MST_COMCODE_Impl();
        	rtnBoolResult = iECC_MST_COMCODE_Impl.updateComCodeList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
