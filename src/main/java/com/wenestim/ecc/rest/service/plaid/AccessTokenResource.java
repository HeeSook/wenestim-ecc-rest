package com.wenestim.ecc.rest.service.plaid;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.plaid.client.PlaidClient;
import com.plaid.client.request.AccountsGetRequest;
import com.plaid.client.request.ItemPublicTokenExchangeRequest;
import com.plaid.client.request.TransactionsGetRequest;
import com.plaid.client.response.Account;
import com.plaid.client.response.AccountsGetResponse;
import com.plaid.client.response.ErrorResponse;
import com.plaid.client.response.ItemPublicTokenExchangeResponse;
import com.plaid.client.response.TransactionsGetResponse;
import com.wenestim.ecc.rest.impl.common.ECC_MST_USER_PLAID_Impl;
import com.wenestim.ecc.rest.util.AppUtil;
import com.wenestim.ecc.rest.util.StringUtil;

import retrofit2.Response;


@Path("/plaid")
public class AccessTokenResource
{
	private final static Logger logger = LogManager.getLogger(AccessTokenResource.class);

	//private PlaidClient plaidClient;
//	private PlaidClient plaidClient = PlaidClient.newBuilder()
//			  .clientIdAndSecret(clientId, secret)
//			  .publicKey(publicKey) // optional. only needed to call endpoints that require a public key
//			  .sandboxBaseUrl() // or equivalent, depending on which environment you're calling into
//			  .build();

	// test secret: 3596c066e1917f7493f88a7b6908d2
	// dev  secret: 3725c7d6fe48263350f5631b8b9e75
	/**
	 * Deprecated 07/23/2020
	 */
	private PlaidClient plaidClient = PlaidClient.newBuilder()
			  .clientIdAndSecret("5e0fc38906c4240013566938", "3725c7d6fe48263350f5631b8b9e75")
			  .publicKey("61fb15d668640f9265d9c19c0b1c69") // optional. only needed to call endpoints that require a public key
			  .developmentBaseUrl() //.sandboxBaseUrl() // or equivalent, depending on which environment you're calling into
			  .build();

	public static String accessToken;

	@POST
	@Path("/set_access_token")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public ItemPublicTokenExchangeResponse getAccessToken(InputStream inputStream) throws IOException, JSONException
//	public String getAccessToken(InputStream inputStream) throws IOException
	{
		logger.info("get_access_token..[1]");
		Boolean rtnBoolResult = Boolean.valueOf(false);

		HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);
		String publicToken = (String)dataMap.get("public_token") ;

//		String clientId    = AppUtil.getPropertyByName("plaid.ClientID");
//		String plaidSecret = AppUtil.getPropertyByName("plaid.Secret");
		String publicKey   = AppUtil.getPropertyByName("plaid.PublicKey");

//		logger.info("clientId:"+clientId);
//		logger.info("secret:"+plaidSecret);
		logger.info("publicKey:"+publicKey);
		logger.info("publicToken:"+publicToken);

		// Use builder to create a client
//		PlaidClient plaidClient = PlaidClient.newBuilder()
//		  .clientIdAndSecret(clientId, secret)
//		  .publicKey(publicKey) // optional. only needed to call endpoints that require a public key
//		  .sandboxBaseUrl() // or equivalent, depending on which environment you're calling into
//		  .build();
//		PlaidClient plaidClient = PlaidClient.newBuilder()
//				  .clientIdAndSecret("5e0fc38906c4240013566938", "3596c066e1917f7493f88a7b6908d2")
//				  .publicKey(publicKey) // optional. only needed to call endpoints that require a public key
//				  .sandboxBaseUrl() // or equivalent, depending on which environment you're calling into
//				  .build();
//
		logger.info("get_access_token..[2]");
		// Synchronously exchange a Link public_token for an API access_token
		// Required request parameters are always Request object constructor arguments
		Response<ItemPublicTokenExchangeResponse> response = plaidClient.service()
		    .itemPublicTokenExchange(new ItemPublicTokenExchangeRequest(publicToken)).execute();

		logger.info("[2] response.code():"+response.code());
		logger.info("[2] response.headers():"+response.headers());
		logger.info("[2] response.message():"+response.message());
		logger.info("[2] response.body():"+response.body());
		logger.info("[2] response.errorBody():"+response.errorBody());

		if (response.isSuccessful()) {
			logger.info("get_access_token..[2] isSuccessful !!");

			accessToken = response.body().getAccessToken();

//	    	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			ArrayList<HashMap<String,Object>> paramList = new ArrayList<HashMap<String,Object>>();

			JSONArray jsArray = new JSONArray((String)dataMap.get("dataItem"));
			for (int i=0; i<jsArray.length(); i++)
			{

				HashMap<String,Object> paramMap = new HashMap<String,Object>();
				JSONObject jsonObject = jsArray.getJSONObject(i);
				Iterator iterator = jsonObject.keys();
				while (iterator.hasNext())
				{
				    String key = iterator.next().toString();
				    String value = jsonObject.has(key) ? jsonObject.getString(key) : "";
				    if( value != null && value.equals("") )
					{
						value = null;
					}
				    paramMap.put(key, value);
				    logger.info(key + ":"+value);

				}
				paramMap.put("ACC_TOKEN", accessToken);
				paramMap.put("PUB_TOKEN", publicToken);
				paramList.add(paramMap);
			}

	    	dataMap.put("dataList", paramList);


			logger.info("[2] accessToken:"+accessToken);
			logger.info("[2] item_id:"+response.body().getItemId());
			logger.info("[2] request_id:"+response.body().getRequestId());
			logger.info("[2] status_code:"+response.code());
			logger.info("[2] response:"+response.toString());

			ECC_MST_USER_PLAID_Impl iECC_MST_USER_PLAID_Impl = new ECC_MST_USER_PLAID_Impl();
			rtnBoolResult = iECC_MST_USER_PLAID_Impl.updateUserAccToken(dataMap);

			logger.info("rtnBoolResult:"+rtnBoolResult);

//			JsonObject rtnJson = new JsonObject();
//			rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");
		}

		logger.info("get_access_token..[3]");

		// Asynchronously do the same thing. Useful for potentially long-lived calls.
		/*
		plaidClient.service()
		    .itemPublicTokenExchange(new ItemPublicTokenExchangeRequest(publicToken))
		    .enqueue(new Callback<ItemPublicTokenExchangeResponse>() {
		        @Override
		        public void onResponse(Call<ItemPublicTokenExchangeResponse> call, Response<ItemPublicTokenExchangeResponse> response) {

		    		logger.info("[3] response.code():"+response.code());
		    		logger.info("[3] response.headers():"+response.headers());
		    		logger.info("[3] response.message():"+response.message());
		    		logger.info("[3] response.body():"+response.body());
		    		logger.info("[3] response.errorBody():"+response.errorBody());

		          if (response.isSuccessful()) {
		            accessToken = response.body().getAccessToken();

		            logger.info("[3] accessToken:"+accessToken);
					logger.info("[3] item_id:"+response.body().getItemId());
					logger.info("[3] request_id:"+response.body().getRequestId());
					logger.info("[3] status_code:"+response.code());
					logger.info("[3] response:"+response.toString());


		          }
		        }

		        @Override
		        public void onFailure(Call<ItemPublicTokenExchangeResponse> call, Throwable t) {
		          // handle the failure as needed
		        	logger.info("[3] accessToken onFailure.. ");
		        	logger.info("[3] "+ call.toString());
		        }
		    });
		*/

		return response.body();
	}

	//@GET
	//@Path("/transactions")
	//@Produces(MediaType.APPLICATION_JSON)
	@POST
	@Path("/transactions")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public TransactionsGetResponse getTransactions(InputStream inputStream) throws IOException, ParseException
	{
		logger.info("[getTransactions] get-transactions-data..[1]");
		HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);
		accessToken = (String)dataMap.get("access_token") ;

		logger.info("[getTransactions] accessToken:"+dataMap.toString());
		logger.info("[getTransactions] accessToken:"+accessToken);
		logger.info("[getTransactions] START_DATE:"+(String)dataMap.get("START_DATE"));
		logger.info("[getTransactions] END_DATE:"+(String)dataMap.get("END_DATE"));

		String START_DATE = (String)dataMap.get("START_DATE");
		String END_DATE   = (String)dataMap.get("END_DATE");

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		Date sDate = formatter.parse(START_DATE);
		Date eDate = formatter.parse(END_DATE);

		logger.info("[getTransactions] sDate:"+sDate);
		logger.info("[getTransactions] eDate:"+eDate);

		/*Date startDate = new Date(System.currentTimeMillis() - 86400000L * 100);
	    Date endDate = new Date();
	    logger.info("[startDate] :"+startDate);
	    logger.info("[endDate] :"+endDate);*/

	    //access-sandbox-f509e869-56d9-4f73-866b-a55c0bcf1aa8
	    TransactionsGetRequest request =
	      new TransactionsGetRequest(accessToken, sDate, eDate)
	        .withCount(100);

	    Response<TransactionsGetResponse> response = null;
	    for (int i = 0; i < 5; i++) {
	      response = plaidClient.service().transactionsGet(request).execute();

	      if (response.isSuccessful()) {

	    	  logger.info("get-transactions-data..[2] SUCCESS !!!");
	    	  break;

	      } else {

	    	  try {
	    		  ErrorResponse errorResponse = plaidClient.parseError(response);

	    		  logger.error("get-transactions-data..[3] ERROR !!!");
	    		  logger.error(errorResponse.toString());
	    		  Thread.sleep(3000);

	    	  } catch(InterruptedException e) {
	    		  // catch error
	    		  logger.error("get-transactions-data..[4] ERROR CATCH !!!");
		          logger.error(e.toString());
		      }

	      }
	    }

	    return response.body();
	}

	@POST
	@Path("/accounts")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public List<Account> getAccounts(InputStream inputStream) throws IOException, ParseException
	{
		logger.info("[getAccounts] get-accounts-data..[1]");
		HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);
		accessToken = (String)dataMap.get("access_token") ;

		logger.info("[getAccounts] accessToken:"+dataMap.toString());
		logger.info("[getAccounts] accessToken:"+accessToken);

	    Response<AccountsGetResponse> response =
	    		plaidClient.
	    			service().
	    				accountsGet(new AccountsGetRequest(accessToken))
	    		.execute();

	    List<Account> accounts = response.body().getAccounts();

	    return accounts;
	}

}
