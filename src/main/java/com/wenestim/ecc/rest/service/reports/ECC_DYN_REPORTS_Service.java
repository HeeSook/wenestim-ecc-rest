package com.wenestim.ecc.rest.service.reports;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.ecc.rest.impl.reports.ECC_DYN_REPORTS_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Reports")
public class ECC_DYN_REPORTS_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_REPORTS_Service.class);

	@POST
    @Path("/TrialBalance/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getTrialBalanceList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_REPORTS_Service.getTrialBalanceList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_REPORTS_Impl iECC_DYN_REPORTS_Impl = new ECC_DYN_REPORTS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_REPORTS_Impl.getTrialBalanceList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/GLItem/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getGLItemList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_REPORTS_Service.getGLItemList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_REPORTS_Impl iECC_DYN_REPORTS_Impl = new ECC_DYN_REPORTS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_REPORTS_Impl.getGLItemList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/SLItem/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getSLItemList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_REPORTS_Service.getSLItemList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_REPORTS_Impl iECC_DYN_REPORTS_Impl = new ECC_DYN_REPORTS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_REPORTS_Impl.getSLItemList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/PLCostCenter/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPLCostConeterList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_REPORTS_Service.getPLCostConeterList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);
        	String costCenterItem    = (String)paramMap.get("costCenterItem") ;
        	ArrayList costCenterList = StringUtil.getJsonToHashMapList(costCenterItem);
        	paramMap.put("costCenterList", costCenterList);

        	ECC_DYN_REPORTS_Impl iECC_DYN_REPORTS_Impl = new ECC_DYN_REPORTS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_REPORTS_Impl.getPLCostConeterList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
