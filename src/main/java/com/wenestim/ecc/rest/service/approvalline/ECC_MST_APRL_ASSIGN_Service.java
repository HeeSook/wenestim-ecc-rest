package com.wenestim.ecc.rest.service.approvalline;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.approvalline.ECC_MST_APRL_ASSIGN_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/ApprovalLine")
public class ECC_MST_APRL_ASSIGN_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_APRL_ASSIGN_Service.class);

	@POST
    @Path("/ApprovalType/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getAprlLineList(InputStream inputStream)
    {
    	logger.info("ECC_MST_APRL_ASSIGN_Service.getAprlLineList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_APRL_ASSIGN_Impl iECC_MST_APRL_ASSIGN_Impl = new ECC_MST_APRL_ASSIGN_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_APRL_ASSIGN_Impl.getAprlLineList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/ApprovalAssign/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getAprlAssingList(InputStream inputStream)
    {
    	logger.info("ECC_MST_APRL_ASSIGN_Service.getAprlAssingList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_APRL_ASSIGN_Impl iECC_MST_APRL_ASSIGN_Impl = new ECC_MST_APRL_ASSIGN_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_APRL_ASSIGN_Impl.getAprlAssingList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("ApprovalAssign/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateAprlAssingList(InputStream inputStream)
    {
		logger.info("ECC_MST_APRL_ASSIGN_Service.updateAprlAssingList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

			String reqItem = (String) dataMap.get("reqItem");
			String cenItem = (String) dataMap.get("cenItem");
        	ArrayList reqList = StringUtil.getJsonToHashMapList(reqItem);
        	ArrayList cenList = StringUtil.getJsonToHashMapList(cenItem);
			dataMap.put("reqList", reqList);
			dataMap.put("cenList", cenList);

        	ECC_MST_APRL_ASSIGN_Impl iECC_MST_APRL_ASSIGN_Impl = new ECC_MST_APRL_ASSIGN_Impl();
        	rtnBoolResult = iECC_MST_APRL_ASSIGN_Impl.updateAprlAssingList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
