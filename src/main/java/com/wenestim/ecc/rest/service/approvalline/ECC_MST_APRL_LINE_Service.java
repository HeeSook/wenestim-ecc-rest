package com.wenestim.ecc.rest.service.approvalline;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.approvalline.ECC_MST_APRL_LINE_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/ApprovalType")
public class ECC_MST_APRL_LINE_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_APRL_LINE_Service.class);


	@POST
	@Path("/Header/List")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getApprovalTypeHeaderList(InputStream inputStream)
	{
		logger.info("ECC_MST_APRL_LINE_Service.getApprovalTypeHeaderList() called");;
		String result = "";

		try
		{
			HashMap<String, Object> paramMap = StringUtil.getParamMap(inputStream);

			ECC_MST_APRL_LINE_Impl iECC_MST_APRL_LINE_Impl = new ECC_MST_APRL_LINE_Impl();
			List<HashMap<String, Object>> resultList = iECC_MST_APRL_LINE_Impl.getApprovalTypeHeaderList(paramMap);

			Gson gson = new Gson();
			if (resultList != null)
			{
				result = gson.toJson(resultList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/getApprovalLineCode")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getApprovalLineCode (InputStream inputStream)
	{
		logger.info("ECC_MST_APRL_LINE_Service.getApprovalLineCode() called");

		String result = "";

		try
		{
			HashMap<String, Object> paramMap = StringUtil.getParamMap(inputStream);

			ECC_MST_APRL_LINE_Impl iECC_MST_APRL_LINE_Impl = new ECC_MST_APRL_LINE_Impl();
			String rtnCode = iECC_MST_APRL_LINE_Impl.getApprovalLineCode(paramMap);


			Gson gson = new Gson();

			if (rtnCode != null)
			{
				result = gson.toJson(rtnCode);
			}

			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Header/Insert")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response insertApprovalTypeHeaderList(InputStream inputStream)
	{
		logger.info("ECC_MST_APRL_LINE_Service.insertApprovalTypeHeaderList() called");
		Boolean rtnBoolResult = Boolean.valueOf(false);

		try
		{
			HashMap<String, Object> dataMap = StringUtil.getParamMap(inputStream);

			String dataItem = (String) dataMap.get("dataItem");
			ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			dataMap.put("dataList", dataList);

			ECC_MST_APRL_LINE_Impl iECC_MST_APRL_LINE_Impl = new ECC_MST_APRL_LINE_Impl();
			rtnBoolResult = iECC_MST_APRL_LINE_Impl.insertApprovalTypeHeaderList(dataMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Header/Update")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateApprovalTypeHeaderList(InputStream inputStream)
	{
		logger.info("ECC_MST_APRL_LINE_Service.updateApprovalTypeList() called");
		Boolean rtnBoolResult = Boolean.valueOf(false);

		try
		{
			HashMap<String, Object> dataMap = StringUtil.getParamMap(inputStream);

			String dataItem = (String) dataMap.get("dataItem");
			ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			dataMap.put("dataList", dataList);

			ECC_MST_APRL_LINE_Impl iECC_MST_APRL_LINE_Impl = new ECC_MST_APRL_LINE_Impl();
			rtnBoolResult = iECC_MST_APRL_LINE_Impl.updateApprovalTypeHeaderList(dataMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Detail/List")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getApprovalTypeDetailList(InputStream inputStream)
	{
		logger.info("ECC_MST_APRL_LINE_Service.getApprovalTypeDetailList() called");;
		String result = "";

		try
		{
			HashMap<String, Object> paramMap = StringUtil.getParamMap(inputStream);

			ECC_MST_APRL_LINE_Impl iECC_MST_APRL_LINE_Impl = new ECC_MST_APRL_LINE_Impl();
			List<HashMap<String, Object>> resultList = iECC_MST_APRL_LINE_Impl.getApprovalTypeDetailList(paramMap);

			Gson gson = new Gson();
			if (resultList != null)
			{
				result = gson.toJson(resultList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Detail/Update")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateApprovalTypeDetailList(InputStream inputStream)
	{
		logger.info("ECC_MST_APRL_LINE_Service.updateApprovalTypeDetailList() called");
		Boolean rtnBoolResult = Boolean.valueOf(false);

		try
		{
			HashMap<String, Object> dataMap = StringUtil.getParamMap(inputStream);

			String dataItem = (String) dataMap.get("dataItem");
			ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			dataMap.put("dataList", dataList);

			ECC_MST_APRL_LINE_Impl iECC_MST_APRL_LINE_Impl = new ECC_MST_APRL_LINE_Impl();
			rtnBoolResult = iECC_MST_APRL_LINE_Impl.updateApprovalTypeDetailList(dataMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}

	}

	@POST
	@Path("/Detail/Delete")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response deleteApprovalTypeDetailList(InputStream inputStream)
	{
		logger.info("ECC_MST_APRL_LINE_Service.deleteApprovalTypeDetailList() called");
		Boolean rtnBoolResult = false;

		try
		{
			HashMap<String, Object> dataMap = StringUtil.getParamMap(inputStream);

			String dataItem = (String) dataMap.get("dataItem");
			ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			dataMap.put("dataList", dataList);

			ECC_MST_APRL_LINE_Impl iECC_MST_APRL_LINE_Impl = new ECC_MST_APRL_LINE_Impl();
			rtnBoolResult = iECC_MST_APRL_LINE_Impl.deleteApprovalTypeDetailList(dataMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}
}