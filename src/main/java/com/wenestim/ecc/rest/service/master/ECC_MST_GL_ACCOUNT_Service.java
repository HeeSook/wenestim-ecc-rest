package com.wenestim.ecc.rest.service.master;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.master.ECC_MST_GL_ACCOUNT_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Master/GLAccount")
public class ECC_MST_GL_ACCOUNT_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_GL_ACCOUNT_Service.class);

	@POST
    @Path("/CoaGlAccount/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getCoaGlAccountList(InputStream inputStream)
    {
    	logger.info("ECC_MST_GL_ACCOUNT_Service.getCoaGlAccountList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_GL_ACCOUNT_Impl iECC_MST_GL_ACCOUNT_Impl = new ECC_MST_GL_ACCOUNT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_GL_ACCOUNT_Impl.getCoaGlAccountList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/CompanyGlAccount/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getComapnyGlAccountList(InputStream inputStream)
    {
    	logger.info("ECC_MST_GL_ACCOUNT_Service.CompanyGlAccountList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_GL_ACCOUNT_Impl iECC_MST_GL_ACCOUNT_Impl = new ECC_MST_GL_ACCOUNT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_GL_ACCOUNT_Impl.getCompanyGlAccountList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/CoaGlAccount/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateCoaGlAccountList(InputStream inputStream)
    {
		logger.info("ECC_MST_GL_ACCOUNT_Service.updateCoaGlAccountList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_GL_ACCOUNT_Impl iECC_MST_GL_ACCOUNT_Impl = new ECC_MST_GL_ACCOUNT_Impl();
        	rtnBoolResult = iECC_MST_GL_ACCOUNT_Impl.updateCoaGlAccountList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/CompanyGlAccount/Create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response createCompanyGlAccountList(InputStream inputStream)
    {
		logger.info("ECC_MST_GL_ACCOUNT_Service.createCompanyGlAccountList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_GL_ACCOUNT_Impl iECC_MST_GL_ACCOUNT_Impl = new ECC_MST_GL_ACCOUNT_Impl();
        	rtnBoolResult = iECC_MST_GL_ACCOUNT_Impl.createCompanyGlAccountList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/CompanyGlAccount/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateCompanyGlAccountList(InputStream inputStream)
    {
		logger.info("ECC_MST_GL_ACCOUNT_Service.updateCompanyGlAccountList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_GL_ACCOUNT_Impl iECC_MST_GL_ACCOUNT_Impl = new ECC_MST_GL_ACCOUNT_Impl();
        	rtnBoolResult = iECC_MST_GL_ACCOUNT_Impl.updateCompanyGlAccountList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/CoaGlAccount/Delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteCoaGlAccountList(InputStream inputStream)
    {
		logger.info("ECC_MST_GL_ACCOUNT_Service.deleteCoaGlAccountList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_GL_ACCOUNT_Impl iECC_MST_GL_ACCOUNT_Impl = new ECC_MST_GL_ACCOUNT_Impl();
        	rtnBoolResult = iECC_MST_GL_ACCOUNT_Impl.deleteCoaGlAccountList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/CompanyGlAccount/Delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteCompanyGlAccountList(InputStream inputStream)
    {
		logger.info("ECC_MST_GL_ACCOUNT_Service.deleteCompanyGlAccountList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_GL_ACCOUNT_Impl iECC_MST_GL_ACCOUNT_Impl = new ECC_MST_GL_ACCOUNT_Impl();
        	rtnBoolResult = iECC_MST_GL_ACCOUNT_Impl.deleteCompanyGlAccountList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Coa/ExcelUpload/CheckData")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getCoaExcelUploadCheckList(InputStream inputStream)
    {
    	logger.info("ECC_MST_GL_ACCOUNT_Service.getCoaExcelUploadCheckList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

        	ECC_MST_GL_ACCOUNT_Impl iECC_MST_GL_ACCOUNT_Impl = new ECC_MST_GL_ACCOUNT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_GL_ACCOUNT_Impl.getCoaExcelUploadCheckList(paramMap);

        	Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Coa/ExcelUpload/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateCoaExcelUploadDataList(InputStream inputStream)
    {
		logger.info("ECC_MST_GL_ACCOUNT_Service.updateCoaExcelUploadDataList() called");

        return updateCoaGlAccountList(inputStream);
    }
}
