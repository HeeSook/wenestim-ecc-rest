package com.wenestim.ecc.rest.service.summary;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.ecc.rest.impl.summary.ECC_SMR_DEFAULT_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Summary/Default")
public class ECC_SMR_DEFAULT_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_SMR_DEFAULT_Service.class);

	@POST
    @Path("/DocStatus/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDocStatusList(InputStream inputStream)
    {
    	logger.info("ECC_SMR_DEFAULT_Service.getDocStatusList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_SMR_DEFAULT_Impl iECC_SMR_DEFAULT_Impl = new ECC_SMR_DEFAULT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_SMR_DEFAULT_Impl.getDocStatusList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/ApprovalWait/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getApprovalWaitList(InputStream inputStream)
    {
    	logger.info("ECC_SMR_DEFAULT_Service.getApprovalWaitList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_SMR_DEFAULT_Impl iECC_SMR_DEFAULT_Impl = new ECC_SMR_DEFAULT_Impl();
        	HashMap<String,Object> resultMap = iECC_SMR_DEFAULT_Impl.getApprovalWaitList(paramMap);

            Gson gson = new Gson();
            if (resultMap != null)
            {
                result = gson.toJson(resultMap);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/ProfitLoss/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getProfitLossList(InputStream inputStream)
    {
    	logger.info("ECC_SMR_DEFAULT_Service.getProfitLossList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_SMR_DEFAULT_Impl iECC_SMR_DEFAULT_Impl = new ECC_SMR_DEFAULT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_SMR_DEFAULT_Impl.getProfitLossList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/BottleNeck/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getBottleNeckList(InputStream inputStream)
    {
    	logger.info("ECC_SMR_DEFAULT_Service.getBottleNeckList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_SMR_DEFAULT_Impl iECC_SMR_DEFAULT_Impl = new ECC_SMR_DEFAULT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_SMR_DEFAULT_Impl.getBottleNeckList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
