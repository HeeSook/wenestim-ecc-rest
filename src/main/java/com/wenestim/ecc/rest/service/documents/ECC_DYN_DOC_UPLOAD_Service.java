package com.wenestim.ecc.rest.service.documents;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.ecc.rest.impl.common.ECC_MST_CODERULE_Impl;
import com.wenestim.ecc.rest.impl.documents.ECC_DYN_DOC_UPLOAD_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Documents/Upload")
public class ECC_DYN_DOC_UPLOAD_Service
{
    private final static Logger logger = LogManager.getLogger(ECC_DYN_DOC_UPLOAD_Service.class);

    @POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getUploadList(InputStream inputStream)
    {
        logger.info("ECC_DYN_DOC_UPLOAD_Service.getUploadList() called");

        String result = "";
        Boolean rtnBoolResult = Boolean.valueOf(false);
        List<HashMap<String,Object>> resultList = null;

        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

            String seqNo       = paramMap.containsKey("SEQ_NO"   ) ? (String)paramMap.get("SEQ_NO"   ) : "";
            String workType    = paramMap.containsKey("WORK_TYPE") ? (String)paramMap.get("WORK_TYPE") : "";
            String dataItem    = (String)paramMap.get("dataItem") ;
            ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
            paramMap.put("dataList",dataList);

            if( workType.equals("CHECK") && (seqNo == null || seqNo.equals("")))
            {
            	ECC_MST_CODERULE_Impl iECC_MST_CODERULE_Impl = new ECC_MST_CODERULE_Impl();
            	paramMap.put("CATEGORY2", "UPLOAD");
            	paramMap.put("CODE"     , "SEQ_NO");

            	seqNo = iECC_MST_CODERULE_Impl.getMasterCode(paramMap);
            	paramMap.put("SEQ_NO", new Integer(seqNo));
            }

            ECC_DYN_DOC_UPLOAD_Impl impl = new ECC_DYN_DOC_UPLOAD_Impl();
            rtnBoolResult = impl.updateUploadList(paramMap);
            if( rtnBoolResult )
            {
            	resultList = impl.getUploadList(paramMap);
            }

            Gson gson = new Gson();
            if (resultList != null)
            {
            	result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}