package com.wenestim.ecc.rest.service.payment;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.payment.ECC_DYN_CLEARING_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Payment/ClearingPosting")
public class ECC_DYN_CLEARING_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_CLEARING_Service.class);

	@POST
    @Path("/OpenItem/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getOpenItemList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_CLEARING_Service.getOpenItemList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);
        	String simulationItem  = (String)paramMap.get("simulationItem") ;
        	ArrayList dataList     = StringUtil.getJsonToHashMapList(simulationItem);
        	paramMap.put("dataList", dataList);

        	ECC_DYN_CLEARING_Impl iECC_DYN_CLEARING_Impl = new ECC_DYN_CLEARING_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_CLEARING_Impl.getOpenItemList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Clearing/Post")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response postClearingList(InputStream inputStream)
    {
		logger.info("ECC_DYN_CLEARING_Service.postClearingList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_DYN_CLEARING_Impl iECC_DYN_CLEARING_Impl = new ECC_DYN_CLEARING_Impl();
        	rtnBoolResult = iECC_DYN_CLEARING_Impl.postClearingList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
