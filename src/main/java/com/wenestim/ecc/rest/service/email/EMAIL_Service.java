package com.wenestim.ecc.rest.service.email;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.email.EMAIL_Impl;
import com.wenestim.ecc.rest.util.StringUtil;
import com.wenestim.ecc.rest.util.mail.SendEmail;

@Path("/Email")
public class EMAIL_Service
{
	private final static Logger logger = LogManager.getLogger(EMAIL_Service.class);

	private HashMap<String,Object> createTestData(){
		HashMap<String,Object> dataMap = new HashMap();
		dataMap.put("CLIENT_ID","ATNS");
		dataMap.put("USER_ID", "eric");
		dataMap.put("COMPANY_CODE","1000");

		ArrayList dataList = new ArrayList();
		HashMap<String,Object> dataItem = new HashMap();

		dataItem.put("POST_YEAR","2019");
		dataItem.put("DOC_NO","21000033");

		dataList.add(dataItem);

		dataItem = new HashMap();

		dataItem.put("POST_YEAR","2019");
		dataItem.put("DOC_NO","21000029");

		dataList.add(dataItem);

		dataItem = new HashMap();

		dataItem.put("POST_YEAR","2019");
		dataItem.put("DOC_NO","21000028");

		dataList.add(dataItem);

		dataItem = new HashMap();

		dataItem.put("POST_YEAR","2019");
		dataItem.put("DOC_NO","21000027");

		dataList.add(dataItem);

		dataItem = new HashMap();

		dataItem.put("POST_YEAR","2019");
		dataItem.put("DOC_NO","21000023");

		dataList.add(dataItem);

		dataMap.put("dataList", dataList);

		return dataMap;
	}

	@POST
	@Path("/SendEmail")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response sendEmail(InputStream inputStream)
	{
		logger.info("EMAIL_Service.sendEmail() called");

		String result = "";
		try
		{
			HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);
			String dataItem = (String)dataMap.get("dataItem");
			ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			dataMap.put("dataList", dataList);

			EMAIL_Impl iEMAIL_Impl = new EMAIL_Impl();
			List<HashMap<String,Object>> resultList = iEMAIL_Impl.getEmailData(createTestData());
			//List<HashMap<String,Object>> resultList = iEMAIL_Impl.getEmailData(dataMap);

			Gson gson = new Gson();
			if( resultList.size() > 0 )
			{
				String resultString = gson.toJson(resultList);
				logger.info("resultString: "+resultString);
				logger.info("substring: "+resultString.substring(0,2));
				logger.info("test: "+!resultString.substring(0,2).equals("[{"));
				if(!resultString.substring(0,2).equals("[{")){
					JSONObject jsObj = new JSONObject(resultString);
					SendEmail.sendEmail(jsObj, "approvalResult");//email info and form choice
				} else {
					String newStr = resultString.substring(1,resultString.length()-1);
					logger.info("NewStr:\n"+newStr);
					String[] resultMany = newStr.split("}\\,\\{");
					logger.info("Split:\n"+String.join("\n",resultMany));
					for (int i=0; i<resultMany.length;i++){
						String str = "";
					String str2 = "}";
						if(i>0) str = "{";
						if(i==resultMany.length) str2 = "";
						logger.info("result:\n"+str+resultMany[i]+str2);
						JSONObject jsObj = new JSONObject(str+resultMany[i]+str2);
						SendEmail.sendEmail(jsObj, "approvalResult");
					}
				}
			}
			if (resultList != null)
			{
				result = gson.toJson(resultList);
			}
			return Response.status(200).entity(result).build();

		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	public void sendEmail(HashMap inputStream)
	{
		logger.info("EMAIL_Service.sendEmail() called");

		String result = "";
		try
		{
			//HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);
			//String dataItem = (String)dataMap.get("dataItem");
			//ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			//dataMap.put("dataList", dataList);

			EMAIL_Impl iEMAIL_Impl = new EMAIL_Impl();
			List<HashMap<String,Object>> resultList = iEMAIL_Impl.getEmailData(inputStream);
			if( resultList.size() > 0 )
			{
				Gson gson = new Gson();
				String resultString = gson.toJson(resultList);
				logger.info("resultString: "+resultString);
				logger.info("substring: "+resultString.substring(0,2));
				logger.info("test: "+!resultString.substring(0,2).equals("[{"));

				if(!resultString.substring(0,2).equals("[{")){
					JSONObject jsObj = new JSONObject(resultString);
					SendEmail.sendEmail(jsObj, "approvalResult");//email info and form choice
				} else if (resultString.equals("[]")) {

				} else {
					String newStr = resultString.substring(1,resultString.length()-1);
					logger.info("NewStr:\n"+newStr);
					String[] resultMany = newStr.split("}\\,\\{");
					logger.info("Split:\n"+String.join("\n",resultMany));
					for (int i=0; i<resultMany.length;i++){
						String str = "";
						String str2 = "}";
						if(i>0) str = "{";
						if(i==resultMany.length) str2 = "";
						logger.info("result:\n"+str+resultMany[i]+str2);
						JSONObject jsObj = new JSONObject(str+resultMany[i]+str2);
						SendEmail.sendEmail(jsObj, "approvalResult");
					}
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
		}
	}

	@GET
	@Path("/User/Status")
	public Response sendUserStatusList() throws Exception
	{
		logger.info("EMAIL_Service.sendUserStatusList() called");
		Boolean rtnBoolResult = Boolean.valueOf(false);
		String result = "";

		try
		{
			EMAIL_Impl iEMAIL_Impl          = new EMAIL_Impl();
			HashMap<String,Object> paramMap = new HashMap<String,Object>();
			List<HashMap<String,Object>> dataList = iEMAIL_Impl.getUserStatusData(paramMap);

			if( dataList.size() > 0 )
			{
				HashMap<String,Object> mailItem = new HashMap<String,Object>();
				mailItem.put("EMAIL_TITLE", "[Wenestim] E-Accounting User Status");
				mailItem.put("EMAIL_TO"   , "kyungcheol.min@atnsusa.com,jake.byun@atnsusa.com,hsheo@atnsusa.com");
				mailItem.put("dataList"   , dataList);
				SendEmail.sendEmailForm(mailItem, "userStatus");
			}

			JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult,"");

            return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			logger.info("Exception: "+e.getMessage());
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}


}