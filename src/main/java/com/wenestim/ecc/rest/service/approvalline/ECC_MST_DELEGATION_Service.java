package com.wenestim.ecc.rest.service.approvalline;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.approvalline.ECC_MST_DELEGATION_Impl;
import com.wenestim.ecc.rest.util.StringUtil;


@Path("/Delegation")
public class ECC_MST_DELEGATION_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_DELEGATION_Service.class);

	@POST
	@Path("/Management/List")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getDelegationListByApprovalUser(InputStream inputStream)
	{
		logger.info("ECC_MST_DELEGATION_Service.getApprovalTypeHeaderList() called");;
		String result = "";

		try
		{
			HashMap<String, Object> paramMap = StringUtil.getParamMap(inputStream);
			ECC_MST_DELEGATION_Impl iECC_MST_DELEGATION_Impl = new ECC_MST_DELEGATION_Impl();
			List<HashMap<String, Object>> resultList = iECC_MST_DELEGATION_Impl.getDelegationListByApprovalUser(paramMap);

			Gson gson = new Gson();
			if (resultList != null)
			{
				result = gson.toJson(resultList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Management/Insert")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response insertDelegationList(InputStream inputStream)
	{
		logger.info("ECC_MST_DELEGATION_Service.insertDelegationList() called");
		Boolean rtnBoolResult = Boolean.valueOf(false);

		try
		{
			HashMap<String, Object> dataMap = StringUtil.getParamMap(inputStream);

			String dataItem = (String) dataMap.get("dataItem");
			ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			dataMap.put("dataList", dataList);

			ECC_MST_DELEGATION_Impl iECC_MST_DELEGATION_Impl = new ECC_MST_DELEGATION_Impl();
			rtnBoolResult = iECC_MST_DELEGATION_Impl.insertDelegationList(dataMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Management/Update")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateDelegationList(InputStream inputStream)
	{
		logger.info("ECC_MS_DELEGATION_Service.updateDelegationList() called");
		Boolean rtnBoolResult = Boolean.valueOf(false);

		try
		{
			HashMap<String, Object> dataMap = StringUtil.getParamMap(inputStream);

			String dataItem = (String) dataMap.get("dataItem");
			ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			dataMap.put("dataList", dataList);

			ECC_MST_DELEGATION_Impl iECC_MST_DELEGATION_Impl = new ECC_MST_DELEGATION_Impl();
			rtnBoolResult = iECC_MST_DELEGATION_Impl.updateDelegationList(dataMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Management/Cancel")
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response cancelDelegation(InputStream inputStream)
	{
		logger.info("ECC_MST_DELGATION_SERVICE.cancelDelegation() called");
		Boolean rtnBoolResult = Boolean.valueOf(false);

		try
		{
			HashMap<String, Object> dataMap = StringUtil.getParamMap(inputStream);

			String dataItem = (String) dataMap.get("dataItem");
			ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			dataMap.put("dataList", dataList);

			ECC_MST_DELEGATION_Impl iECC_MST_DELEGATION_Impl = new ECC_MST_DELEGATION_Impl();
			rtnBoolResult = iECC_MST_DELEGATION_Impl.cancelDelegation(dataMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}
}