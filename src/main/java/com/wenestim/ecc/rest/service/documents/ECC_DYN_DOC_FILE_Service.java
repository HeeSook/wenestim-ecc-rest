package com.wenestim.ecc.rest.service.documents;


import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.documents.ECC_DYN_DOC_FILE_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Documents/File")
public class ECC_DYN_DOC_FILE_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_DOC_FILE_Service.class);

	@POST
    @Path("/FileList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getHeaderList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_FILE_Service.getHeaderList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_FILE_Impl iECC_DYN_DOC_FILE_Impl = new ECC_DYN_DOC_FILE_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_FILE_Impl.getFileList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/FileUpdate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateFiles(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_FILE_Service.updateFiles() called");

    	Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);
        	String attachItem    = (String)dataMap.get("attachItem") ;
        	ArrayList attachList = StringUtil.getJsonToHashMapList(attachItem);
            dataMap.put("attachList", attachList);



        	ECC_DYN_DOC_FILE_Impl iECC_DYN_DOC_FILE_Impl = new ECC_DYN_DOC_FILE_Impl();
        	rtnBoolResult = iECC_DYN_DOC_FILE_Impl.updateFiles(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult,"");

            return Response.status(200).entity(rtnJson.toString()).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@GET
    @Path("/download")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadPdfFile(
	    		@QueryParam("CLIENT_ID"            ) String CLIENT_ID         ,
	            @QueryParam("COMPANY_CODE"         ) String COMPANY_CODE      ,
	            @QueryParam("DOC_NO"               ) String DOC_NO            ,
	            @QueryParam("FILE_PATH"            ) String FILE_PATH            ,
	            @QueryParam("NAME"                 ) String NAME
    		) throws UnsupportedEncodingException
    {
		CLIENT_ID          = StringUtil.getEmptyNullPrevString(CLIENT_ID         ) ;
        COMPANY_CODE       = StringUtil.getEmptyNullPrevString(COMPANY_CODE      ) ;
        DOC_NO             = StringUtil.getEmptyNullPrevString(DOC_NO            ) ;
        NAME               = URLDecoder.decode(StringUtil.getEmptyNullPrevString(NAME),"UTF-8") ;
        FILE_PATH          = StringUtil.getEmptyNullPrevString(FILE_PATH              ) ;

        File file = new File("c://ecc/" + FILE_PATH + "/" + DOC_NO + "/" + NAME);
	    return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
	        .header("Content-Disposition", "attachment; filename=\"" + NAME + "\"")
	        .build();
    }

}
