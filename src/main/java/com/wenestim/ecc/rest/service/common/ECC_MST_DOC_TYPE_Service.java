package com.wenestim.ecc.rest.service.common;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.ecc.rest.impl.common.ECC_MST_DOC_TYPE_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Common/DocType")
public class ECC_MST_DOC_TYPE_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_DOC_TYPE_Service.class);

	@POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDocTypeList(InputStream inputStream)
    {
    	logger.info("ECC_MST_DOC_TYPE_Service.getDocTypeList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_DOC_TYPE_Impl iECC_MST_DOC_TYPE_Impl = new ECC_MST_DOC_TYPE_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_DOC_TYPE_Impl.getDocTypeList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
