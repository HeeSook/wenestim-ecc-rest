package com.wenestim.ecc.rest.service.payment;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.payment.ECC_DYN_CHECK_ISSUE_Impl;
import com.wenestim.ecc.rest.util.StringUtil;


@Path("/Payment/Check")
public class ECC_DYN_CHECK_ISSUE_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_CHECK_ISSUE_Service.class);

	@POST
    @Path("/CheckIssue/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getCheckIssueList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_CHECK_ISSUE_Service.getCheckIssueList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_CHECK_ISSUE_Impl iECC_DYN_CHECK_ISSUE_Impl = new ECC_DYN_CHECK_ISSUE_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_CHECK_ISSUE_Impl.getCheckIssueList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/CheckIssued/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getCheckIssuedList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_CHECK_ISSUE_Service.getCheckIssuedList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_CHECK_ISSUE_Impl iECC_DYN_CHECK_ISSUE_Impl = new ECC_DYN_CHECK_ISSUE_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_CHECK_ISSUE_Impl.getCheckIssuedList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/CheckPrint/HeaderList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getCheckPrintHeaderList(InputStream inputStream)
	{
		logger.info("ECC_DYN_CHECK_ISSUE_Service.getCheckPrintHeaderList() called");

		String result = "";
		try
		{
			HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);
			System.out.println(paramMap);
        	ECC_DYN_CHECK_ISSUE_Impl iECC_DYN_CHECK_ISSUE_Impl = new ECC_DYN_CHECK_ISSUE_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_CHECK_ISSUE_Impl.getCheckPrintHeadList(paramMap);

        	Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
    @Path("/CheckPrint/ItemList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getCheckPrintItemList (InputStream inputStream)
	{
		logger.info("ECC_DYN_CHECK_ISSUE_Service.getCheckPrintItemList() called");

		String result = "";
		try
		{
			HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);
			System.out.println(paramMap);
        	ECC_DYN_CHECK_ISSUE_Impl iECC_DYN_CHECK_ISSUE_Impl = new ECC_DYN_CHECK_ISSUE_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_CHECK_ISSUE_Impl.getCheckPrintItemList(paramMap);

        	Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
    @Path("/CheckAssign/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateCheckAssignList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_CHECK_ISSUE_Service.updateCheckAssignList() called");
    	Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String issueItem    = (String)dataMap.get("issueItem") ;
        	ArrayList issueList = StringUtil.getJsonToHashMapList(issueItem);
        	dataMap.put("issueList", issueList);

        	String bookItem    = (String)dataMap.get("bookItem") ;
        	ArrayList bookList = StringUtil.getJsonToHashMapList(bookItem);
        	dataMap.put("bookList" , bookList);

        	ECC_DYN_CHECK_ISSUE_Impl iECC_DYN_CHECK_ISSUE_Impl = new ECC_DYN_CHECK_ISSUE_Impl();
        	rtnBoolResult = iECC_DYN_CHECK_ISSUE_Impl.updateCheckAssignList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/CheckStatus/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateCheckStatusList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_CHECK_ISSUE_Service.updateCheckStatusList() called");
    	Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_DYN_CHECK_ISSUE_Impl iECC_DYN_CHECK_ISSUE_Impl = new ECC_DYN_CHECK_ISSUE_Impl();
        	rtnBoolResult = iECC_DYN_CHECK_ISSUE_Impl.updateCheckStatusList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

//	@POST
//    @Path("/Void")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
//    public Response voidCheckList(InputStream inputStream)
//    {
//    	logger.info("ECC_DYN_CHECK_ISSUE_Service.voidCheckList() called");
//    	Boolean rtnBoolResult = Boolean.valueOf(false);
//        try
//        {
//        	ECC_DYN_CHECK_ISSUE_Impl iECC_DYN_CHECK_ISSUE_Impl = new ECC_DYN_CHECK_ISSUE_Impl();
//        	ECC_MST_CHECK_Impl       iECC_MST_CHECK_Impl       = new ECC_MST_CHECK_Impl();
//        	JsonObject rtnJson = new JsonObject();
//        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);
//
//        	String dataItem    = (String)dataMap.get("dataItem") ;
//        	ArrayList<HashMap<String,Object>> dataList = StringUtil.getJsonToHashMapList(dataItem);
//        	dataMap.put("dataList", dataList);
//
////        	iECC_MST_CHECK_Impl.updateCheckNo(dataMap);
//        	rtnBoolResult = iECC_DYN_CHECK_ISSUE_Impl.voidCheckList(dataMap);
//
//            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");
//
//            return Response.status(200).entity(rtnJson.toString()).build();
//        }
//        catch (Exception e)
//        {
//        	logger.error("Exception:" + e.getMessage() + "#");
//            e.printStackTrace();
//            return Response.status(500).entity(e.toString()).build();
//        }
//    }

}
