package com.wenestim.ecc.rest.service.payment;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.payment.ECC_DYN_PAYMENT_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Payment/PaymentPosting")
public class ECC_DYN_PAYMENT_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_PAYMENT_Service.class);

	@POST
    @Path("/OpenItem/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getOpenItemList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_PAYMENT_Service.getOpenItemList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_PAYMENT_Impl iECC_DYN_PAYMENT_Impl = new ECC_DYN_PAYMENT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_PAYMENT_Impl.getOpenItemList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Simulation/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getSimulationList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_PAYMENT_Service.getSimulationList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

        	ECC_DYN_PAYMENT_Impl iECC_DYN_PAYMENT_Impl = new ECC_DYN_PAYMENT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_PAYMENT_Impl.getSimulationList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Payment/Post")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response postPaymentList(InputStream inputStream)
    {
		logger.info("ECC_DYN_PAYMENT_Service.postPaymentList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String openItem    = (String)dataMap.get("openItem") ;
        	String postItem    = (String)dataMap.get("postItem") ;

        	ArrayList openList = StringUtil.getJsonToHashMapList(openItem);
        	ArrayList postList = StringUtil.getJsonToHashMapList(postItem);

        	dataMap.put("openList", openList);
        	dataMap.put("postList", postList);

        	ECC_DYN_PAYMENT_Impl iECC_DYN_PAYMENT_Impl = new ECC_DYN_PAYMENT_Impl();
        	rtnBoolResult = iECC_DYN_PAYMENT_Impl.postPaymentList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/InterPayment/Post")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response postInterPaymentList(InputStream inputStream)
    {
		logger.info("ECC_DYN_PAYMENT_Service.postPaymentList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String openItem    = (String)dataMap.get("openItem") ;
        	String postItem    = (String)dataMap.get("postItem") ;

        	ArrayList openList = StringUtil.getJsonToHashMapList(openItem);
        	ArrayList postList = StringUtil.getJsonToHashMapList(postItem);

        	dataMap.put("openList", openList);
        	dataMap.put("postList", postList);

        	ECC_DYN_PAYMENT_Impl iECC_DYN_PAYMENT_Impl = new ECC_DYN_PAYMENT_Impl();
        	rtnBoolResult = iECC_DYN_PAYMENT_Impl.postInterPaymentList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/PostingResult/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPostingResultList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_PAYMENT_Service.getPostingResultList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_PAYMENT_Impl iECC_DYN_PAYMENT_Impl = new ECC_DYN_PAYMENT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_PAYMENT_Impl.getPostingResultList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Payment/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPaymentList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_PAYMENT_Service.getPaymentList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_PAYMENT_Impl iECC_DYN_PAYMENT_Impl = new ECC_DYN_PAYMENT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_PAYMENT_Impl.getPaymentList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/IsOpenItem/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getIsOpenItemList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_PAYMENT_Service.getIsOpenItemList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

        	ECC_DYN_PAYMENT_Impl iECC_DYN_PAYMENT_Impl = new ECC_DYN_PAYMENT_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_PAYMENT_Impl.getIsOpenItemList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

}
