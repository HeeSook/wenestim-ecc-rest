package com.wenestim.ecc.rest.service.documents;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.documents.ECC_DYN_DOC_APRL_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Documents/Aprl")
public class ECC_DYN_DOC_APRL_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_DOC_APRL_Service.class);

	@POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getAprlList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_APRL_Service.getAprlList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_APRL_Impl iECC_DYN_DOC_APRL_Impl = new ECC_DYN_DOC_APRL_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_APRL_Impl.getAprlList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/FavoriteList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getFavoriteAprlList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_APRL_Service.getFavoriteAprlList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_APRL_Impl iECC_DYN_DOC_APRL_Impl = new ECC_DYN_DOC_APRL_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_APRL_Impl.getFavoriteAprlList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/DefaultList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDefaultAprlList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_APRL_Service.getDefaultAprlList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_APRL_Impl iECC_DYN_DOC_APRL_Impl = new ECC_DYN_DOC_APRL_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_APRL_Impl.getDefaultAprlList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Favorite/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateFavoriteAprl(InputStream inputStream)
    {
		logger.info("ECC_DYN_DOC_LIST_Service.updateFavoriteAprl() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_DYN_DOC_APRL_Impl iECC_DYN_DOC_APRL_Impl = new ECC_DYN_DOC_APRL_Impl();
        	rtnBoolResult = iECC_DYN_DOC_APRL_Impl.updateFavoriteAprl(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

}
