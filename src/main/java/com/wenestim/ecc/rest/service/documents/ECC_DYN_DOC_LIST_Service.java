package com.wenestim.ecc.rest.service.documents;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.common.ECC_MST_CODERULE_Impl;
import com.wenestim.ecc.rest.impl.documents.ECC_DYN_DOC_LIST_Impl;
import com.wenestim.ecc.rest.service.email.EMAIL_Service;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Documents")
public class ECC_DYN_DOC_LIST_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_DOC_LIST_Service.class);

	@POST
    @Path("/DefaultDoc/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDefaultDocList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_LIST_Service.getDefaultDocList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_LIST_Impl.getDefaultDocList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/InterDoc/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getInterTransactionDocList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_LIST_Service.getInterTransactionDocList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_LIST_Impl.getInterTransactionDocList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/ApproverDoc/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getApproverDocList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_LIST_Service.getApproverDocList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_LIST_Impl.getApproverDocList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/DelegateDoc/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDelegateDocList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_LIST_Service.getDelegateDocList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_LIST_Impl.getDelegateDocList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/DocReverse/NewNo/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDocReverseNewNoList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_LIST_Service.getDocReverseNewNoList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_LIST_Impl.getDocReverseNewNoList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/InterItem/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getInterTrItemList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_LIST_Service.getInterTrItemList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_LIST_Impl.getInterTrItemList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/OpenItem/getFlag")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getOpenItemFlag(InputStream inputStream)
    {
    	logger.info("ECC_DYN_DOC_LIST_Service.getOpenItemFlag() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_DOC_LIST_Impl.getOpenItemFlag(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/DocApproval/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateDocApprovalList(InputStream inputStream)
    {
		logger.info("ECC_DYN_DOC_LIST_Service.updateDocApprovalList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	rtnBoolResult = iECC_DYN_DOC_LIST_Impl.updateDocApprovalList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

			EMAIL_Service iEMAIL_Service = new EMAIL_Service();
			iEMAIL_Service.sendEmail(dataMap);

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/DocAdditional/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateDocAdditionalList(InputStream inputStream)
    {
		logger.info("ECC_DYN_DOC_LIST_Service.updateDocAdditionalList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	rtnBoolResult = iECC_DYN_DOC_LIST_Impl.updateDocAdditionalList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/DocReverse/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateDocReverseList(InputStream inputStream)
    {
		logger.info("ECC_DYN_DOC_LIST_Service.updateDocReverseList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	ECC_MST_CODERULE_Impl iECC_MST_CODERULE_Impl = new ECC_MST_CODERULE_Impl();
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String headerItem    = (String)dataMap.get("headerItem") ;
        	String dataItem      = (String)dataMap.get("dataItem"  ) ;
        	ArrayList headerList = StringUtil.getJsonToHashMapList(headerItem);
        	ArrayList dataList   = StringUtil.getJsonToHashMapList(dataItem  );

    		for(HashMap<String,Object> companyItem : (ArrayList<HashMap<String,Object>>)headerList)
    		{
    			String COMPANY_CODE = (String)companyItem.get("COMPANY_CODE");
    			companyItem.put("SEQ_NO", Integer.parseInt(iECC_MST_CODERULE_Impl.getMasterCode(dataMap)));

    		}
    		dataMap.put("headerList",headerList);
    		dataMap.put("dataList"  ,dataList  );

    		ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	rtnBoolResult = iECC_DYN_DOC_LIST_Impl.updateDocReverseList(dataMap);

    		JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/DocReset/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateDocResetList(InputStream inputStream)
    {
		logger.info("ECC_DYN_DOC_LIST_Service.updateDocResetList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	rtnBoolResult = iECC_DYN_DOC_LIST_Impl.updateDocResetList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/DocMultiReq/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateDocMultiReqList(InputStream inputStream)
    {
		logger.info("ECC_DYN_DOC_LIST_Service.updateDocMultiReqList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_DYN_DOC_LIST_Impl iECC_DYN_DOC_LIST_Impl = new ECC_DYN_DOC_LIST_Impl();
        	rtnBoolResult = iECC_DYN_DOC_LIST_Impl.updateDocMultiReqList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

			EMAIL_Service iEMAIL_Service = new EMAIL_Service();
			iEMAIL_Service.sendEmail(dataMap);

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	private ArrayList<HashMap<String,Object>> getCompanyItemList(String companyCode, ArrayList<HashMap<String,Object>> dataList)
    {
        ArrayList<HashMap<String,Object>> rtnList = new ArrayList<HashMap<String,Object>>();

		for(HashMap<String,Object> dataItem : dataList)
		{
			String itemCompany = (String)dataItem.get("COMPANY_CODE");
			if( companyCode.equals(itemCompany))
			{
				rtnList.add(dataItem);
			}
		}
        return rtnList;
    }
}
