package com.wenestim.ecc.rest.service.master;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.master.ECC_MST_FS_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Master/FS")
public class ECC_MST_FS_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_FS_Service.class);

	@POST
    @Path("/All/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getFinancialStatementHierarchyList(InputStream inputStream)
    {
    	logger.info("ECC_MST_FS_Service.getFinancialStatementHierarchyList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_FS_Impl iECC_MST_FS_Impl = new ECC_MST_FS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_FS_Impl.getFinancialStatementHierarchyList(paramMap);

        	resultList = StringUtil.getIncludeNullField(resultList,"PARENT_ID");

        	GsonBuilder gsonBuilder = new GsonBuilder();
        	gsonBuilder.serializeNulls();
        	Gson gson = gsonBuilder.create();

            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/CheckFSCodeExists")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response checkFSCodeExists(InputStream inputStream)
    {
    	logger.info("ECC_MST_FS_Service.checkFSCodeExists() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_FS_Impl iECC_MST_FS_Impl = new ECC_MST_FS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_FS_Impl.checkFSCodeExists(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Parent/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getFSParentCodeList(InputStream inputStream)
    {
    	logger.info("ECC_MST_FS_Service.getFSParentCodeList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_FS_Impl iECC_MST_FS_Impl = new ECC_MST_FS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_FS_Impl.getFSParentCodeList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Child/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getFSChildCodeList(InputStream inputStream)
    {
    	logger.info("ECC_MST_FS_Service.getFSChildCodeList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_FS_Impl iECC_MST_FS_Impl = new ECC_MST_FS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_FS_Impl.getFSChildCodeList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/existsFixName")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response existsFixName(InputStream inputStream)
    {
    	logger.info("ECC_MST_FS_Service.checkFSCodeExists() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_FS_Impl iECC_MST_FS_Impl = new ECC_MST_FS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_FS_Impl.existsFixName(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Version/Create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response createFS(InputStream inputStream)
    {
		logger.info("ECC_MST_FS_Service.createPlan() called");
        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_FS_Impl iECC_MST_FS_Impl = new ECC_MST_FS_Impl();
        	String FSVer = iECC_MST_FS_Impl.createFS(paramMap);
        	if( !FSVer.equals(""))
        	{
        		rtnBoolResult = true;
        	}

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, FSVer);

            return Response.status(200).entity(rtnJson.toString()).build();

        } catch (Exception e) {
        	logger.error("Exception:" + e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response insertFSMaster(InputStream inputStream)
    {
    	logger.info("ECC_MST_FS_Service.insertFSMaster() called");
    	Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_FS_Impl iECC_MST_FS_Impl = new ECC_MST_FS_Impl();
        	rtnBoolResult = iECC_MST_FS_Impl.insertFSMaster(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateFSMaster(InputStream inputStream)
    {
    	logger.info("ECC_MST_FS_Service.updateFSMaster() called");
    	Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_FS_Impl iECC_MST_FS_Impl = new ECC_MST_FS_Impl();
        	rtnBoolResult = iECC_MST_FS_Impl.updateFSMaster(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteFSMaster(InputStream inputStream)
    {
    	logger.info("ECC_MST_FS_Service.deleteFSMaster() called");
    	Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_FS_Impl iECC_MST_FS_Impl = new ECC_MST_FS_Impl();
        	rtnBoolResult = iECC_MST_FS_Impl.deleteFSMaster(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
