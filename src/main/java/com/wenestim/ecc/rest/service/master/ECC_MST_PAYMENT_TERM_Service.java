package com.wenestim.ecc.rest.service.master;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.master.ECC_MST_PAYMENT_TERM_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Master/PaymentTerm")
public class ECC_MST_PAYMENT_TERM_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_PAYMENT_TERM_Service.class);

	@POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPaymentTermList(InputStream inputStream)
    {
    	logger.info("ECC_MST_PAYMENT_TERM_Service.getPaymentTermList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_PAYMENT_TERM_Impl iECC_MST_PAYMENT_TERM_Impl = new ECC_MST_PAYMENT_TERM_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_PAYMENT_TERM_Impl.getPaymentTermList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updatePaymentTermList(InputStream inputStream)
    {
    	logger.info("ECC_MST_PAYMENT_TERM_Service.updatePaymentTermList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_PAYMENT_TERM_Impl iECC_MST_PAYMENT_TERM_Impl = new ECC_MST_PAYMENT_TERM_Impl();
        	rtnBoolResult = iECC_MST_PAYMENT_TERM_Impl.updatePaymentTermList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deletePaymentTermList(InputStream inputStream)
    {
    	logger.info("ECC_MST_PAYMENT_TERM_Service.deletePaymentTermList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_PAYMENT_TERM_Impl iECC_MST_PAYMENT_TERM_Impl = new ECC_MST_PAYMENT_TERM_Impl();
        	rtnBoolResult = iECC_MST_PAYMENT_TERM_Impl.deletePaymentTermList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
