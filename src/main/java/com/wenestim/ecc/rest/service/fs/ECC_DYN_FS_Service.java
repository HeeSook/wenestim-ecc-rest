package com.wenestim.ecc.rest.service.fs;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.ecc.rest.impl.fs.ECC_DYN_FS_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/FS")
public class ECC_DYN_FS_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_FS_Service.class);

	@POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getFinancialStatementList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_FS_Service.getFinancialStatementList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_FS_Impl iECC_DYN_FS_Impl = new ECC_DYN_FS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_FS_Impl.getFinancialStatementList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Consolidation/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getConsolidationList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_FS_Service.getConsolidationList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);
        	logger.info(paramMap);
        	ECC_DYN_FS_Impl iECC_DYN_FS_Impl = new ECC_DYN_FS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_FS_Impl.getConsolidationList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Dashboard/Data/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDashboardDataList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_FS_Service.getDashboardDataList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_FS_Impl iECC_DYN_FS_Impl = new ECC_DYN_FS_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_FS_Impl.getDashboardDataList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
