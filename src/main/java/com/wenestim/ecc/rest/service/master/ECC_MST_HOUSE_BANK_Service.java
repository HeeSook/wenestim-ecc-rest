package com.wenestim.ecc.rest.service.master;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.master.ECC_MST_HOUSE_BANK_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Master/HouseBank")
public class ECC_MST_HOUSE_BANK_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_HOUSE_BANK_Service.class);

	@POST
    @Path("/Header/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getBankHeaderList(InputStream inputStream)
    {
    	logger.info("ECC_MST_HOUSE_BANK_Service.getBankHeaderList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_HOUSE_BANK_Impl iECC_MST_HOUSE_BANK_Impl = new ECC_MST_HOUSE_BANK_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_HOUSE_BANK_Impl.getBankHeaderList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Detail/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getBankDetailList(InputStream inputStream)
    {
    	logger.info("ECC_MST_HOUSE_BANK_Service.getBankDetailList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_HOUSE_BANK_Impl iECC_MST_HOUSE_BANK_Impl = new ECC_MST_HOUSE_BANK_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_HOUSE_BANK_Impl.getBankDetailList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/AccId/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getBankAccIdList(InputStream inputStream)
    {
    	logger.info("ECC_MST_HOUSE_BANK_Service.getBankAccIdList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_HOUSE_BANK_Impl iECC_MST_HOUSE_BANK_Impl = new ECC_MST_HOUSE_BANK_Impl();
        	List<HashMap<String,Object>> resultList = iECC_MST_HOUSE_BANK_Impl.getBankAccIdList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Header/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateBankHeaderList(InputStream inputStream)
    {
		logger.info("ECC_MST_HOUSE_BANK_Service.updateBankHeaderList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_HOUSE_BANK_Impl iECC_MST_HOUSE_BANK_Impl = new ECC_MST_HOUSE_BANK_Impl();
        	rtnBoolResult = iECC_MST_HOUSE_BANK_Impl.updateBankHeaderList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Detail/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateBankDetailList(InputStream inputStream)
    {
		logger.info("ECC_MST_HOUSE_BANK_Service.updateBankDetailList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_HOUSE_BANK_Impl iECC_MST_HOUSE_BANK_Impl = new ECC_MST_HOUSE_BANK_Impl();
        	rtnBoolResult = iECC_MST_HOUSE_BANK_Impl.updateBankDetailList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Header/Delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteBankHeaderList(InputStream inputStream)
    {
		logger.info("ECC_MST_HOUSE_BANK_Service.deleteBankHeaderList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_HOUSE_BANK_Impl iECC_MST_HOUSE_BANK_Impl = new ECC_MST_HOUSE_BANK_Impl();
        	rtnBoolResult = iECC_MST_HOUSE_BANK_Impl.deleteBankHeaderList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Detail/Delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteBankDetailList(InputStream inputStream)
    {
		logger.info("ECC_MST_HOUSE_BANK_Service.deleteBankDetailList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)dataMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	dataMap.put("dataList", dataList);

        	ECC_MST_HOUSE_BANK_Impl iECC_MST_HOUSE_BANK_Impl = new ECC_MST_HOUSE_BANK_Impl();
        	rtnBoolResult = iECC_MST_HOUSE_BANK_Impl.deleteBankDetailList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
