package com.wenestim.ecc.rest.service.common;

import java.io.InputStream;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.ecc.rest.impl.common.ECC_MST_CODERULE_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Common/CodeRule")
public class ECC_MST_CODERULE_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_CODERULE_Service.class);

	@POST
    @Path("/Code")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getMasterCode(InputStream inputStream)
    {
    	logger.info("ECC_MST_CODERULE_Service.Code() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_MST_CODERULE_Impl iECC_MST_CODERULE_Impl = new ECC_MST_CODERULE_Impl();
        	String rtnCode = iECC_MST_CODERULE_Impl.getMasterCode(paramMap);

            Gson gson = new Gson();
            if (rtnCode != null)
            {
                result = gson.toJson(rtnCode);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/DocNo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDocNo(InputStream inputStream)
    {
    	logger.info("ECC_MST_CODERULE_Service.getDocNo() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String CLIENT_ID    = (String)paramMap.get("CLIENT_ID")   ;
        	String COMPANY_CODE = (String)paramMap.get("COMPANY_CODE");
        	String CATEGORY2    = (String)paramMap.get("CATEGORY2")   ;
        	String CODE         = (String)paramMap.get("CODE")        ;
        	int POST_YEAR       = Integer.parseInt((String)paramMap.get("POST_YEAR"));

        	ECC_MST_CODERULE_Impl iECC_MST_CODERULE_Impl = new ECC_MST_CODERULE_Impl();
        	String rtnCode = iECC_MST_CODERULE_Impl.getDocNo(CLIENT_ID,COMPANY_CODE,CATEGORY2,CODE,POST_YEAR);

            Gson gson = new Gson();
            if (rtnCode != null)
            {
                result = gson.toJson(rtnCode);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
