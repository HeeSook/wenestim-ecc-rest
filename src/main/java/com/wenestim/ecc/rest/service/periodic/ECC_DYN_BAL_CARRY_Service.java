package com.wenestim.ecc.rest.service.periodic;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.periodic.ECC_DYN_BAL_CARRY_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Periodic")
public class ECC_DYN_BAL_CARRY_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_BAL_CARRY_Service.class);

	@POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getBalCarryList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_BAL_CARRY_Service.getBalCarryList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_BAL_CARRY_Impl iECC_DYN_BAL_CARRY_Impl = new ECC_DYN_BAL_CARRY_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_BAL_CARRY_Impl.getBalCarryList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Execute")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response executeBalCarryList(InputStream inputStream)
    {
		logger.info("ECC_DYN_BAL_CARRY_Service.executeBalCarryList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);
        	ECC_DYN_BAL_CARRY_Impl iECC_DYN_BAL_CARRY_Impl = new ECC_DYN_BAL_CARRY_Impl();
        	rtnBoolResult = iECC_DYN_BAL_CARRY_Impl.executeBalCarryList(dataMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
