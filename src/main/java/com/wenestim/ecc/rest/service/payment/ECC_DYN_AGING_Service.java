package com.wenestim.ecc.rest.service.payment;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.ecc.rest.impl.payment.ECC_DYN_AGING_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Payment/Aging")
public class ECC_DYN_AGING_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_AGING_Service.class);

	@POST
    @Path("/Summary/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getAgingSummaryList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_AGING_Service.getAgingSummaryList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_AGING_Impl iECC_DYN_AGING_Impl = new ECC_DYN_AGING_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_AGING_Impl.getAgingSummaryList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


	@POST
    @Path("/Detail/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getAgingDetailList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_AGING_Service.getAgingDetailList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_AGING_Impl iECC_DYN_AGING_Impl = new ECC_DYN_AGING_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_AGING_Impl.getAgingDetailList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
