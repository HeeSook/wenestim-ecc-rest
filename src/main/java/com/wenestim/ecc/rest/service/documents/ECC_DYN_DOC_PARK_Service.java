package com.wenestim.ecc.rest.service.documents;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.common.ECC_MST_CODERULE_Impl;
import com.wenestim.ecc.rest.impl.documents.ECC_DYN_DOC_APRL_Impl;
import com.wenestim.ecc.rest.impl.documents.ECC_DYN_DOC_FILE_Impl;
import com.wenestim.ecc.rest.impl.documents.ECC_DYN_DOC_PARK_Impl;
import com.wenestim.ecc.rest.service.email.EMAIL_Service;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Documents/Park")
public class ECC_DYN_DOC_PARK_Service
{
    private final static Logger logger = LogManager.getLogger(ECC_DYN_DOC_PARK_Service.class);

    @POST
    @Path("/Invoice/Check")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getCheckInvoice(InputStream inputStream)
    {
        logger.info("ECC_DYN_DOC_PARK_Service.getCheckInvoice() called");

        String result = "";
        try
        {
            HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

            ECC_DYN_DOC_PARK_Impl iECC_DYN_DOC_PARK_Impl = new ECC_DYN_DOC_PARK_Impl();
            String rtnCode = iECC_DYN_DOC_PARK_Impl.getCheckInvoice(paramMap);

            Gson gson = new Gson();
            if (rtnCode != null)
            {
                result = gson.toJson(rtnCode);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Period/Check")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getCheckPeriod(InputStream inputStream)
    {
        logger.info("ECC_DYN_DOC_PARK_Service.getCheckPeriod() called");

        String result = "";
        try
        {
            HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

            ECC_DYN_DOC_PARK_Impl iECC_DYN_DOC_PARK_Impl = new ECC_DYN_DOC_PARK_Impl();
            String rtnCode = iECC_DYN_DOC_PARK_Impl.getCheckPeriod(paramMap);

            Gson gson = new Gson();
            if (rtnCode != null)
            {
                result = gson.toJson(rtnCode);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/ExRate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDocExRate(InputStream inputStream)
    {
        logger.info("ECC_DYN_DOC_PARK_Service.getDocExRate() called");

        String result = "";
        try
        {
            HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

            ECC_DYN_DOC_PARK_Impl iECC_DYN_DOC_PARK_Impl = new ECC_DYN_DOC_PARK_Impl();
            HashMap<String,Object> rtnMap = iECC_DYN_DOC_PARK_Impl.getDocExRate(paramMap);

            Gson gson = new Gson();
            if (rtnMap != null)
            {
                result = gson.toJson(rtnMap);
            }
            else
            {
                result = "0";
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/PaymentTerms")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPaymentTerms(InputStream inputStream)
    {
        logger.info("ECC_DYN_DOC_PARK_Service.getPaymentTerms() called");

        String result = "";
        try
        {
            HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

            ECC_DYN_DOC_PARK_Impl iECC_DYN_DOC_PARK_Impl = new ECC_DYN_DOC_PARK_Impl();
            HashMap<String,Object> rtnMap = iECC_DYN_DOC_PARK_Impl.getPaymentTerms(paramMap);

            Gson gson = new Gson();
            if (rtnMap != null)
            {
                result = gson.toJson(rtnMap);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/HeaderList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getHeaderList(InputStream inputStream)
    {
        logger.info("ECC_DYN_DOC_PARK_Service.getHeaderList() called");

        String result = "";
        try
        {
            HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

            ECC_DYN_DOC_PARK_Impl iECC_DYN_DOC_PARK_Impl = new ECC_DYN_DOC_PARK_Impl();
            HashMap<String,Object> resultMap = iECC_DYN_DOC_PARK_Impl.getHeaderList(paramMap);

            Gson gson = new Gson();
            if (resultMap != null)
            {
                result = gson.toJson(resultMap);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/ItemList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getItemList(InputStream inputStream)
    {
        logger.info("ECC_DYN_DOC_PARK_Service.getItemList() called");

        String result = "";
        try
        {
            HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

            ECC_DYN_DOC_PARK_Impl iECC_DYN_DOC_PARK_Impl = new ECC_DYN_DOC_PARK_Impl();
            List<HashMap<String,Object>> resultList = iECC_DYN_DOC_PARK_Impl.getItemList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/ClearList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getClearList(InputStream inputStream)
    {
        logger.info("ECC_DYN_DOC_PARK_Service.getClearList() called");

        String result = "";
        try
        {
            HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

            ECC_DYN_DOC_PARK_Impl iECC_DYN_DOC_PARK_Impl = new ECC_DYN_DOC_PARK_Impl();
            List<HashMap<String,Object>> resultList = iECC_DYN_DOC_PARK_Impl.getClearList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


    @POST
    @Path("/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateParkItems(InputStream inputStream)
    {
        logger.info("ECC_DYN_DOC_PARK_Service.updateParkItems() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	ArrayList<HashMap<String,Object>> companyList = null;
        	HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

            ECC_MST_CODERULE_Impl iECC_MST_CODERULE_Impl = new ECC_MST_CODERULE_Impl();
            ECC_DYN_DOC_PARK_Impl iECC_DYN_DOC_PARK_Impl = new ECC_DYN_DOC_PARK_Impl();
            ECC_DYN_DOC_APRL_Impl iECC_DYN_DOC_APRL_Impl = new ECC_DYN_DOC_APRL_Impl();
            ECC_DYN_DOC_FILE_Impl iECC_DYN_DOC_FILE_Impl = new ECC_DYN_DOC_FILE_Impl();

            boolean directPosting = false;
            String isAvailDoc     = "Y";
            String rtnMsg         = (String)dataMap.get("DOC_NO"    ) ;
            String dataItem       = (String)dataMap.get("dataItem"  ) ;
            String aprlItem       = (String)dataMap.get("aprlItem"  ) ;
            String attachItem     = (String)dataMap.get("attachItem") ;
            ArrayList dataList    = StringUtil.getJsonToHashMapList(dataItem  );
            ArrayList aprlList    = StringUtil.getJsonToHashMapList(aprlItem  );
            ArrayList attachList  = StringUtil.getJsonToHashMapList(attachItem);

            String SUMRY_STATUS = (String)dataMap.get("SUMRY_STATUS");
            String POSTING_FLAG = (String)dataMap.get("POSTING_FLAG");
            String INTER_FLAG   = dataMap.get("INTER_FLAG"  ) == null ? "N" :(String)dataMap.get("INTER_FLAG");

            if( SUMRY_STATUS.equals("APP_REQ") && POSTING_FLAG.equals("Y"))
            {
                directPosting = true;
            }

            dataMap.put("dataList"  , dataList  );
            dataMap.put("aprlList"  , aprlList  );
            dataMap.put("attachList", attachList);

            if((String)dataMap.get("DOC_NO") == null)
            {
            	rtnMsg      = "";
            	companyList = getCompanyList(dataMap);

            	for(HashMap<String,Object> companyItem : companyList)
            	{
            		String companyCode = (String)companyItem.get("COMPANY_CODE");
            		String postYear    = (String)companyItem.get("POST_YEAR"   );
            		String docNo       = (String)companyItem.get("DOC_NO"      );
            		dataMap.put("COMPANY_CODE" , companyCode);
            		dataMap.put("NEW_POST_YEAR", postYear   );
            		dataMap.put("NEW_DOC_NO"   , docNo      );
            		isAvailDoc = iECC_DYN_DOC_PARK_Impl.getCheckDocNo(dataMap);
            		if( isAvailDoc.equals("N") )
            		{
            			rtnBoolResult = false;
            			rtnMsg = "E:"+docNo;
            			break;
            		}
            	}

            	if(isAvailDoc.equals("Y"))
            	{
            		for(HashMap<String,Object> companyItem : companyList)
                    {

                		String companyCode = (String)companyItem.get("COMPANY_CODE");
                		String workType    = (String)companyItem.get("WORK_TYPE"   );
                		String docType     = (String)companyItem.get("DOC_TYPE"    );
                		String docNo       = (String)companyItem.get("DOC_NO"      );
                		String intComType  = (String)companyItem.get("INT_COM_TYPE");
                		dataMap.put("COMPANY_CODE", companyCode);
                    	dataMap.put("WORK_TYPE"   , workType   );
                    	dataMap.put("DOC_TYPE"    , docType    );
                		dataMap.put("DOC_NO"      , docNo      );
                		dataMap.put("INT_COM_TYPE", intComType );
                		dataMap.put("dataList"    , getCompanyItemList(companyCode,dataList));

                		if(	iECC_DYN_DOC_PARK_Impl.updateParkHeader(dataMap) == true &&
                			iECC_DYN_DOC_PARK_Impl.updateParkItems (dataMap) == true &&
                			iECC_DYN_DOC_APRL_Impl.updateParkAprl  (dataMap) == true &&
                            iECC_DYN_DOC_FILE_Impl.updateFiles     (dataMap) == true)
                    	{
                    		rtnBoolResult = true;
                    	}
                    	else
                    	{
                            rtnBoolResult = false;
                        }

                    	if( INTER_FLAG.equals("N"))
                    	{
                    		rtnMsg = docNo;
                    	}
                    	else
                    	{
                    		rtnMsg += companyCode + ":" + docNo+ ",";
                    	}
                    }

                	if( directPosting && rtnBoolResult)
                    {
                		for(HashMap<String,Object> companyItem : companyList)
                		{
                			String companyCode = (String)companyItem.get("COMPANY_CODE");
                			String docNo       = (String)companyItem.get("DOC_NO");

                			dataMap.put("COMPANY_CODE", companyCode);
                        	dataMap.put("DOC_NO"      , docNo      );

                        	rtnBoolResult = iECC_DYN_DOC_PARK_Impl.updateDirectPosting(dataMap);
                		}
                    }
            	}
            }
            else
            {
            	String clientID    = (String)dataMap.get("CLIENT_ID"    );
                String companyCode = (String)dataMap.get("COMPANY_CODE" );
            	String isCreate    = (String)dataMap.get("IS_CREATE"    ) ;
            	String orgPostYear = (String)dataMap.get("ORG_POST_YEAR") ;
            	String orgDocNo    = (String)dataMap.get("ORG_DOC_NO"   ) ;
            	String orgFilePath = (String)dataMap.get("ORG_FILE_PATH") ;
            	String newPostYear = (String)dataMap.get("POST_YEAR"    ) ;
            	String newDocType  = (String)dataMap.get("DOC_TYPE"     ) ;
            	String newFilePath = (String)dataMap.get("FILE_PATH"    ) ;
            	String newDocNo    = iECC_MST_CODERULE_Impl.getDocNo( clientID,companyCode,newDocType,newDocType,Integer.parseInt(newPostYear));

            	if( isCreate.equals("Y"))
            	{
            		dataMap.put("NEW_POST_YEAR", newPostYear);
            		dataMap.put("NEW_DOC_NO"   , newDocNo   );
            		isAvailDoc = iECC_DYN_DOC_PARK_Impl.getCheckDocNo(dataMap);
            		if(isAvailDoc.equals("N"))
            		{
            			rtnBoolResult = false;
            			rtnMsg = "E:"+newDocNo;
            		}
            		else
            		{
            			dataMap.put("POST_YEAR", orgPostYear);
            		}
            	}

            	if(isAvailDoc.equals("Y"))
            	{
	            	if(iECC_DYN_DOC_PARK_Impl.deleteParkItems(dataMap) == true)
	                {
	                	if( isCreate.equals("Y"))
	                	{
	                		File oldPath = new File("c://ecc/" + orgFilePath + "/" + orgDocNo);
	                		File newPath = new File("c://ecc/" + newFilePath + "/" + newDocNo);

	                		if(oldPath.exists())
	                		{
	                			oldPath.renameTo(newPath);
	                		}
	                		rtnMsg = newDocNo;

	                		dataMap.put("POST_YEAR", newPostYear);
	                		dataMap.put("DOC_NO"   , newDocNo   );
	                	}
	            		if(attachList.size() == 0)
	                    {
	                    	if(	iECC_DYN_DOC_PARK_Impl.updateParkHeader(dataMap) == true &&
	                    		iECC_DYN_DOC_PARK_Impl.updateParkItems (dataMap) == true &&
	                    		iECC_DYN_DOC_APRL_Impl.updateParkAprl(dataMap)   == true)
	                    	{
	                            rtnBoolResult = true;
	                            if( directPosting )
	                            {
	                                rtnBoolResult = iECC_DYN_DOC_PARK_Impl.updateDirectPosting(dataMap);
	                            }
	                        }
	                    	else
	                    	{
	                            rtnBoolResult = false;
	                        }
	                    }
	                    else
	                    {
	                        if(	iECC_DYN_DOC_PARK_Impl.updateParkHeader(dataMap) == true &&
	                    		iECC_DYN_DOC_PARK_Impl.updateParkItems (dataMap) == true &&
	                    		iECC_DYN_DOC_APRL_Impl.updateParkAprl  (dataMap) == true &&
	                    		iECC_DYN_DOC_FILE_Impl.updateFiles     (dataMap) == true )
	                        {
	                            rtnBoolResult = true;
	                            if( directPosting )
	                            {
	                                rtnBoolResult = iECC_DYN_DOC_PARK_Impl.updateDirectPosting(dataMap);
	                            }
	                        }
	                        else
	                        {
	                            rtnBoolResult = false;
	                        }
	                    }
	                }
	            	else
	            	{
	                    rtnBoolResult = false;
	                }
            	}
            }

            if(!dataMap.get("SUMRY_STATUS").toString().equals("SAVE") && rtnBoolResult)
            {
            	if( INTER_FLAG.equals("Y"))
            	{
            		sendMail((String)dataMap.get("FROM_COMPANY"), (String)dataMap.get("FROM_DOC_NO"), dataMap);
            		sendMail((String)dataMap.get("TO_COMPANY"  ), (String)dataMap.get("TO_DOC_NO"  ), dataMap);
            	}
            	else
            	{
            		sendMail((String)dataMap.get("COMPANY_CODE"), (String)dataMap.get("DOC_NO"     ), dataMap);
            	}
            }

            JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult,rtnMsg);

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteParkItems(InputStream inputStream)
    {
        logger.info("ECC_DYN_DOC_PARK_Service.deleteParkItems() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
            HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

            ECC_DYN_DOC_PARK_Impl iECC_DYN_DOC_PARK_Impl = new ECC_DYN_DOC_PARK_Impl();
            rtnBoolResult = iECC_DYN_DOC_PARK_Impl.deleteParkItems(dataMap);

            JsonObject rtnJson = new JsonObject();
            rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    private ArrayList <HashMap<String,Object>> getCompanyList(HashMap<String,Object> dataMap)
    {
    	ECC_MST_CODERULE_Impl iECC_MST_CODERULE_Impl = new ECC_MST_CODERULE_Impl();

        ArrayList<HashMap<String,Object>> companyList = new ArrayList<HashMap<String,Object>>();
        HashMap<String,Object>            companyMap  = null;

        String INT_TRANS_NO = "";
        String INT_COM_TYPE = "";
        String TO_DOC_NO    = "";
        String CLIENT_ID    = (String)dataMap.get("CLIENT_ID"   );
        String COMAPNY_CODE = (String)dataMap.get("COMPANY_CODE");
        String WORK_TYPE    = (String)dataMap.get("WORK_TYPE"   );
        String DOC_TYPE     = (String)dataMap.get("DOC_TYPE"    );
        String POST_YYYY    = (String)dataMap.get("POST_YEAR"   );
        int    POST_YEAR    = Integer.parseInt((String) (dataMap.get("POST_YEAR")));

        String INTER_FLAG   = dataMap.get("INTER_FLAG"  ) == null ? "N" :(String)dataMap.get("INTER_FLAG"  );
        String TO_COMPANY   = dataMap.get("TO_COMPANY"  ) == null ? ""  :(String)dataMap.get("TO_COMPANY"  );
        String TO_WORK_TYPE = dataMap.get("TO_WORK_TYPE") == null ? ""  :(String)dataMap.get("TO_WORK_TYPE");
        String TO_DOC_TYPE  = dataMap.get("TO_DOC_TYPE" ) == null ? ""  :(String)dataMap.get("TO_DOC_TYPE" );

        String DOC_NO = iECC_MST_CODERULE_Impl.getDocNo( CLIENT_ID,COMAPNY_CODE,DOC_TYPE,DOC_TYPE,POST_YEAR);
        if( INTER_FLAG.equals("Y"))
        {
        	INT_COM_TYPE = "FROM";
        }

        companyMap  = new HashMap<String,Object>();
        companyMap.put("COMPANY_CODE", COMAPNY_CODE);
        companyMap.put("WORK_TYPE"   , WORK_TYPE   );
        companyMap.put("DOC_TYPE"    , DOC_TYPE    );
        companyMap.put("POST_YEAR"   , POST_YYYY   );
        companyMap.put("DOC_NO"      , DOC_NO      );
    	companyMap.put("INT_COM_TYPE", INT_COM_TYPE);
        companyList.add(companyMap);

        if(INTER_FLAG.equals("Y"))
    	{
        	INT_TRANS_NO = StringUtil.getIntTransNo(COMAPNY_CODE,DOC_NO,POST_YYYY);
        	TO_DOC_NO    = iECC_MST_CODERULE_Impl.getDocNo( CLIENT_ID,TO_COMPANY,TO_DOC_TYPE,TO_DOC_TYPE,POST_YEAR);

        	companyMap  = new HashMap<String,Object>();
        	companyMap.put("COMPANY_CODE", TO_COMPANY  );
            companyMap.put("WORK_TYPE"   , TO_WORK_TYPE);
            companyMap.put("DOC_TYPE"    , TO_DOC_TYPE );
        	companyMap.put("DOC_NO"      , TO_DOC_NO   );
        	companyMap.put("INT_COM_TYPE", "TO"        );
            companyList.add(companyMap);
    	}

        dataMap.put("FROM_COMPANY", COMAPNY_CODE);
        dataMap.put("FROM_DOC_NO" , DOC_NO      );
        dataMap.put("TO_COMPANY"  , TO_COMPANY  );
        dataMap.put("TO_DOC_NO"   , TO_DOC_NO   );
        dataMap.put("INT_TRANS_NO", INT_TRANS_NO);
        dataMap.put("companyList" , companyList);

        return companyList;
    }

    private ArrayList<HashMap<String,Object>> getCompanyItemList(String companyCode, ArrayList<HashMap<String,Object>> dataList)
    {
        ArrayList<HashMap<String,Object>> rtnList = new ArrayList<HashMap<String,Object>>();

		for(HashMap<String,Object> dataItem : dataList)
		{
			String itemCompany = (String)dataItem.get("COMPANY_CODE");
			if( companyCode.equals(itemCompany))
			{
				rtnList.add(dataItem);
			}
		}
        return rtnList;
    }

    private void sendMail(String companyCode, String docNo, HashMap<String,Object> dataMap)
    {
        ArrayList dataList           = new ArrayList();
    	HashMap<String,Object> param = new HashMap<String,Object>();

    	String postYear = (String)dataMap.get("POST_YEAR");
        param.put("POST_YEAR", postYear);
        param.put("DOC_NO"   , docNo   );
        dataList.add(param);

    	dataMap.put("COMPANY_CODE", companyCode);
        dataMap.put("dataList"    , dataList   );

        EMAIL_Service iEMAIL_Service = new EMAIL_Service();
        iEMAIL_Service.sendEmail(dataMap);
    }

}