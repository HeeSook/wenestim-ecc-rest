package com.wenestim.ecc.rest.service.common;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.ecc.rest.impl.common.ECC_MST_USER_PLAID_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Common/Token")
public class ECC_MST_USER_PLAID_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_MST_USER_PLAID_Service.class);

	@POST
	@Path("/Acc")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getUserAccToken(InputStream inputStream)
	{
		logger.info("ECC_MST_USER_PLAID_Service.getUserAccToken() called");

		String result = "";
		try
		{
			HashMap<String, Object> paramMap = StringUtil.getParamMap(inputStream);

			ECC_MST_USER_PLAID_Impl iECC_MST_USER_PLAID_Impl = new ECC_MST_USER_PLAID_Impl();
			List<HashMap<String,Object>> resultList = iECC_MST_USER_PLAID_Impl.getUserAccToken(paramMap);

			Gson gson = new Gson();
			if (resultList != null)
			{
				result = gson.toJson(resultList);
			}
			return Response.status(200).entity(result).build();

		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}

	}

	@POST
	@Path("/Update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateUserAccToken(InputStream inputStream)
	{
		logger.info("ECC_MST_USER_PLAID_Service.updateUserAccToken() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			HashMap<String,Object> dataMap = StringUtil.getParamMap(inputStream);

			String dataItem    = (String)dataMap.get("dataItem");
			ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
			dataMap.put("dataList", dataList);

			ECC_MST_USER_PLAID_Impl iECC_MST_USER_PLAID_Impl = new ECC_MST_USER_PLAID_Impl();
			rtnBoolResult = iECC_MST_USER_PLAID_Impl.updateUserAccToken(dataMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = StringUtil.setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			logger.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}
}
