package com.wenestim.ecc.rest.service.bank;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.ecc.rest.impl.bank.ECC_DYN_BANK_Impl;
import com.wenestim.ecc.rest.util.StringUtil;

@Path("/Bank")
public class ECC_DYN_BANK_Service
{
	private final static Logger logger = LogManager.getLogger(ECC_DYN_BANK_Service.class);

	@POST
    @Path("/BankStatement/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getBankStatementList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_BANK_Service.getBankStatementList() called");

        String result = "";
        try
        {

        	List<HashMap<String,Object>> resultList    = new ArrayList<HashMap<String,Object>>();

        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_BANK_Impl iECC_DYN_BANK_Impl = new ECC_DYN_BANK_Impl();
        	List<HashMap<String,Object>> clearItemList = iECC_DYN_BANK_Impl.getClearMappingList (paramMap);
        	List<HashMap<String,Object>> bankItemList  = iECC_DYN_BANK_Impl.getBankStatementList(paramMap);

			for(HashMap<String,Object> bankItem : bankItemList)
			{
				resultList.add(bankItem);
				String BANK_STATUS = (String)bankItem.get("BANK_STATUS");
				String POST_YEAR   = (String)bankItem.get("POST_YEAR"  );
				String DOC_NO      = (String)bankItem.get("DOC_NO"     );

				if( DOC_NO.equals(""))
				{
					continue;
				}
				List<HashMap<String,Object>> clearDataList = clearItemList.stream()
		                .filter(item ->	item.get("POST_YEAR").toString().equals(POST_YEAR)
		                		&&      item.get("DOC_NO"   ).toString().equals(DOC_NO   )
		                		)
		                .collect(Collectors.toList());

				for(HashMap<String,Object> clearItem : clearDataList)
				{
					bankItem.put("CLEAR_NO"  , clearItem.get("CLEAR_NO"  ));
					bankItem.put("CLEAR_YEAR", clearItem.get("CLEAR_YEAR"));

					List<HashMap<String,Object>> vendorCustItemList  = iECC_DYN_BANK_Impl.getVendorCustItemList(bankItem);
					resultList.addAll(vendorCustItemList);
				}
			}

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Outstanding/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getOutstandingBalanceList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_BANK_Service.getOutstandingList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_BANK_Impl iECC_DYN_BANK_Impl = new ECC_DYN_BANK_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_BANK_Impl.getOutstandingBalanceList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/AccountBalance/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getAccountBalanceList(InputStream inputStream)
    {
    	logger.info("ECC_DYN_BANK_Service.getAccountBalanceList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	ECC_DYN_BANK_Impl iECC_DYN_BANK_Impl = new ECC_DYN_BANK_Impl();
        	List<HashMap<String,Object>> resultList = iECC_DYN_BANK_Impl.getAccountBalanceList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
        	logger.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

}
