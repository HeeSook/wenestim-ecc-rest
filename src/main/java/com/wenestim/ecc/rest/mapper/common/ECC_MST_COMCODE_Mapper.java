package com.wenestim.ecc.rest.mapper.common;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_COMCODE_Mapper
{
	public List<HashMap<String, Object>> getComCodeList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getComCodeSearchList(HashMap<String, Object> paramMap);

	public Boolean updateComCodeList(HashMap<String, Object> dataMap);
}
