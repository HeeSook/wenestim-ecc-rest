package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_CUSTOMER_Mapper
{
	public List<HashMap<String, Object>> getCustomerList(HashMap<String, Object> paramMap);

	public Boolean updateCustomerList(HashMap<String, Object> dataMap);

	public Boolean deleteCustomerList(HashMap<String, Object> dataMap);

	public List<HashMap<String, Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap);
}
