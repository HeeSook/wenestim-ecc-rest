package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_PERIOD_Mapper
{
	public List<HashMap<String, Object>> getPeriodList(HashMap<String, Object> paramMap);

	public Boolean updatePeriodList(HashMap<String, Object> dataMap);
}
