package com.wenestim.ecc.rest.mapper.documents;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_DOC_FILE_Mapper
{
	public List<HashMap<String, Object>> getFileList(HashMap<String, Object> paramMap);
	
	public Boolean updateFiles(HashMap<String, Object> dataMap);
	
	public Boolean deleteFiles(HashMap<String, Object> dataMap);

}
