package com.wenestim.ecc.rest.mapper.common;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_DOC_TYPE_Mapper
{
	public List<HashMap<String, Object>> getDocTypeList(HashMap<String, Object> paramMap);
}
