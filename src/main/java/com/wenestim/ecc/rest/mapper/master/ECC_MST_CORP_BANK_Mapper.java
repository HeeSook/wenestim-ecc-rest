package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_CORP_BANK_Mapper
{
	public List<HashMap<String, Object>> getBankList(HashMap<String, Object> paramMap);

	public Boolean updateBankList(HashMap<String, Object> dataMap);

	public Boolean deleteBankList(HashMap<String, Object> dataMap);
}
