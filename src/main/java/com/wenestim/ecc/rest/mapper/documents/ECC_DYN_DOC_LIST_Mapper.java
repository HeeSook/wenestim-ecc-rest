package com.wenestim.ecc.rest.mapper.documents;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_DOC_LIST_Mapper
{
	public List<HashMap<String, Object>> getDefaultDocList (HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getInterTransactionDocList (HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getApproverDocList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getDelegateDocList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getDocReverseNewNoList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getInterTrItemList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getOpenItemFlag(HashMap<String, Object> paramMap);

	public Boolean updateDocApprovalList(HashMap<String, Object> dataMap);

	public Boolean updateDocAdditionalList(HashMap<String, Object> dataMap);

	public Boolean updateDocReverseList(HashMap<String, Object> dataMap);

	public Boolean updateDocResetList(HashMap<String, Object> dataMap);

	public Boolean updateDocMultiReqList(HashMap<String, Object> dataMap);

}
