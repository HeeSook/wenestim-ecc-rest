package com.wenestim.ecc.rest.mapper.payment;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_CLEARING_Mapper
{
	public List<HashMap<String, Object>> getOpenItemList(HashMap<String, Object> paramMap);

	public Boolean postClearingList(HashMap<String, Object> dataMap);
}
