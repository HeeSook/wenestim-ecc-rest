package com.wenestim.ecc.rest.mapper.documents;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_DOC_APRL_Mapper
{
	public List<HashMap<String, Object>> getAprlList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getFavoriteAprlList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getDefaultAprlList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> callDefaultAprlList(HashMap<String, Object> paramMap);

	public Boolean updateParkAprl(HashMap<String, Object> dataMap);

	public Boolean updateFavoriteAprl(HashMap<String, Object> dataMap);

}
