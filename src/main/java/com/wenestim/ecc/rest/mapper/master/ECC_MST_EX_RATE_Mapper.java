package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_EX_RATE_Mapper
{
	public List<HashMap<String, Object>> getExRateList(HashMap<String, Object> paramMap);

	public Boolean updateExRateList(HashMap<String, Object> dataMap);

	public Boolean deleteExRateList(HashMap<String, Object> dataMap);
}
