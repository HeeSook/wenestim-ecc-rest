package com.wenestim.ecc.rest.mapper.common;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_COMPANY_Mapper
{
	public List<HashMap<String, Object>> getCompanyList(HashMap<String, Object> paramMap);

	public Boolean updateCompanyList(HashMap<String, Object> dataMap);
}
