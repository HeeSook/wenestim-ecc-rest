package com.wenestim.ecc.rest.mapper.payment;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_AGING_Mapper
{
	public List<HashMap<String, Object>> getAgingSummaryList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getAgingDetailList(HashMap<String, Object> paramMap);
}
