package com.wenestim.ecc.rest.mapper.periodic;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_BAL_CARRY_Mapper
{
	public List<HashMap<String, Object>> getBalCarryList(HashMap<String, Object> paramMap);

	public Boolean executeBalCarryList(HashMap<String, Object> dataMap);

}
