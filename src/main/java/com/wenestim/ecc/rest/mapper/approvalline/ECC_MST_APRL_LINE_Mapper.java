package com.wenestim.ecc.rest.mapper.approvalline;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface ECC_MST_APRL_LINE_Mapper
{
	public List<HashMap<String, Object>> getApprovalTypeHeaderList(HashMap<String, Object> paramMap);
	
	public List<HashMap<String, Object>> getApprovalTypeDetailList (HashMap<String, Object> paramMap);
		
	public String getApprovalLineCode (HashMap<String, Object> paramMap); 
	
	public Boolean insertApprovalTypeHeaderList(HashMap<String, Object> dataMap);
	
	public Boolean updateApprovalTypeHeaderList(HashMap<String, Object> dataMap);
	
	public Boolean updateApprovalTypeDetailList(HashMap<String, Object> dataMap);
	
	public Boolean deleteApprovalTypeDetailList(HashMap<String, Object> dataMap);
}

