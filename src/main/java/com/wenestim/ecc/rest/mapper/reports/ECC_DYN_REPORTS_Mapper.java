package com.wenestim.ecc.rest.mapper.reports;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_REPORTS_Mapper
{
	public List<HashMap<String, Object>> getTrialBalanceList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getGLItemList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getSLItemList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getPLCostConeterList(HashMap<String, Object> paramMap);
}
