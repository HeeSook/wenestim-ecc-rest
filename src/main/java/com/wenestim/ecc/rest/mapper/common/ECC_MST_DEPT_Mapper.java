package com.wenestim.ecc.rest.mapper.common;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_DEPT_Mapper
{
	public List<HashMap<String, Object>> getDeptList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getDeptListHierarchyList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> checkDeptCodeExists(HashMap<String, Object> paramMap);

	public Boolean insertDept(HashMap<String, Object> dataMap);

	public Boolean deleteDept(HashMap<String, Object> dataMap);
	
	public Boolean updateDept(HashMap<String, Object> dataMap);
}
