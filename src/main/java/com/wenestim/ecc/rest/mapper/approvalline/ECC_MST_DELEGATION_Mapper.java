package com.wenestim.ecc.rest.mapper.approvalline;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_DELEGATION_Mapper
{
	public List<HashMap<String, Object>> getDelegationListByApprovalUser(HashMap<String, Object> paramMap);
	
	public Boolean insertDelegationList(HashMap<String, Object> dataMap);
	
	public Boolean updateDelegationList(HashMap<String, Object> dataMap);
	
	public Boolean cancelDelegation(HashMap<String, Object> dataMap);
	
}