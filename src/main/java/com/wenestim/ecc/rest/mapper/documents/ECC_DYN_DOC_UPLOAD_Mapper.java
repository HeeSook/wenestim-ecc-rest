package com.wenestim.ecc.rest.mapper.documents;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_DOC_UPLOAD_Mapper
{
	public List<HashMap<String, Object>> getUploadList(HashMap<String, Object> paramMap);

	public Boolean updateUploadList(HashMap<String, Object> dataMap);
}
