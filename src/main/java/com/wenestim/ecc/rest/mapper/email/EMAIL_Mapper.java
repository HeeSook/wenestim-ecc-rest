package com.wenestim.ecc.rest.mapper.email;

import java.util.HashMap;
import java.util.List;

public interface EMAIL_Mapper
{
	public List<HashMap<String, Object>> getEmailData     (HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getUserStatusData(HashMap<String, Object> paramMap);

}
