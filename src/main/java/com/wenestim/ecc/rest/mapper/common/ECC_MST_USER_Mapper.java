package com.wenestim.ecc.rest.mapper.common;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_USER_Mapper
{
	public List<HashMap<String, Object>> getUserList(HashMap<String, Object> paramMap);

	public Boolean updateUserList(HashMap<String, Object> dataMap);

	public Boolean updatePwResetList(HashMap<String, Object> dataMap);

	public Boolean updateMyInfo(HashMap<String, Object> dataMap);

	public Boolean deleteUserList(HashMap<String, Object> dataMap);
}
