package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_PAYMENT_TERM_Mapper
{
	public List<HashMap<String, Object>> getPaymentTermList(HashMap<String, Object> paramMap);

	public Boolean updatePaymentTermList(HashMap<String, Object> dataMap);

	public Boolean deletePaymentTermList(HashMap<String, Object> dataMap); 
}
