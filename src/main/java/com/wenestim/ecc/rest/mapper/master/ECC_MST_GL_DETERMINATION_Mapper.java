package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_GL_DETERMINATION_Mapper
{
	public List<HashMap<String, Object>> getGlDeterminationInterCompanyList(HashMap<String, Object> paramMap);
	
	public Boolean updateGlDeterminationInterCompanyList(HashMap<String, Object> dataMap);

	public Boolean deleteGlDeterminationInterCompanyList(HashMap<String, Object> dataMap);
}
