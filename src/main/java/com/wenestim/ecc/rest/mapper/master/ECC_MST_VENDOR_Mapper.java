package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_VENDOR_Mapper
{
	public List<HashMap<String, Object>> getVendorList(HashMap<String, Object> paramMap);

	public Boolean updateVendorList(HashMap<String, Object> dataMap);

	public Boolean deleteVendorList(HashMap<String, Object> dataMap);

	public List<HashMap<String, Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap);

}
