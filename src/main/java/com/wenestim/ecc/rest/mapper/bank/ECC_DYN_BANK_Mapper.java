package com.wenestim.ecc.rest.mapper.bank;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_BANK_Mapper
{
	public List<HashMap<String, Object>> getBankStatementList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getOutstandingBalanceList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getClearMappingList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getVendorCustItemList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getAccountBalanceList(HashMap<String, Object> paramMap);
}
