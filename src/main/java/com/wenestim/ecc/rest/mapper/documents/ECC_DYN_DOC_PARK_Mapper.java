package com.wenestim.ecc.rest.mapper.documents;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_DOC_PARK_Mapper
{
	public String getCheckDocNo (HashMap<String, Object> paramMap);

	public String getCheckPeriod (HashMap<String, Object> paramMap);

	public String getCheckInvoice(HashMap<String, Object> paramMap);

	public HashMap<String, Object> getDocExRate(HashMap<String, Object> paramMap);

	public HashMap<String, Object> getPaymentTerms(HashMap<String, Object> paramMap);

	public HashMap<String, Object> getHeaderList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getItemList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getClearList(HashMap<String, Object> paramMap);

	public Boolean updateParkHeader(HashMap<String, Object> dataMap);

	public Boolean updateParkItems(HashMap<String, Object> dataMap);

	public Boolean updateDirectPosting(HashMap<String, Object> dataMap);

	public Boolean deleteParkItems(HashMap<String, Object> dataMap);
}
