package com.wenestim.ecc.rest.mapper.summary;

import java.util.HashMap;
import java.util.List;

public interface ECC_SMR_DEFAULT_Mapper
{
	public List<HashMap<String, Object>> getDocStatusList(HashMap<String, Object> paramMap);

	public HashMap<String,Object> getApprovalWaitList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getProfitLossList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getBottleNeckList(HashMap<String, Object> paramMap);
}
