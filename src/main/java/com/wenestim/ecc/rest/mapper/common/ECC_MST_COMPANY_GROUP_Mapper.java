package com.wenestim.ecc.rest.mapper.common;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_COMPANY_GROUP_Mapper
{
	public List<HashMap<String, Object>> getCompanyGroupList(HashMap<String, Object> paramMap);
	
	public List<HashMap<String, Object>> getCompanyGroupSearchList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getCompanyGroupChildSearchList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getCGParentCodeList(HashMap<String, Object> paramMap);
	
	public List<HashMap<String, Object>> checkCGCodeExists(HashMap<String, Object> paramMap);

	public Boolean createCompanyGroupList(HashMap<String, Object> dataMap);
	
	public Boolean updateCompanyGroupList(HashMap<String, Object> dataMap);
	
	public Boolean deleteCompanyGroupList(HashMap<String, Object> dataMap);
}
