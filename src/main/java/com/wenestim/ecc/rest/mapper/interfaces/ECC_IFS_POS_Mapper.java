package com.wenestim.ecc.rest.mapper.interfaces;

import java.util.HashMap;
import java.util.List;

public interface ECC_IFS_POS_Mapper
{
	public List<HashMap<String, Object>> getPosIdList(HashMap<String, Object> paramMap);
	
	public List<HashMap<String, Object>> getPosInterfaceList(HashMap<String, Object> paramMap);
	
	public Boolean updatePosInterfaceList(HashMap<String, Object> dataMap);
	
	public Boolean postPOSData(HashMap<String, Object> dataMap);
}
