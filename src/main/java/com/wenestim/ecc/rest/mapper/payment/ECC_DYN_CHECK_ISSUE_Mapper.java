package com.wenestim.ecc.rest.mapper.payment;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_CHECK_ISSUE_Mapper
{
	public List<HashMap<String, Object>> getCheckIssueList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getCheckIssuedList(HashMap<String, Object> paramMap);
	
	public List<HashMap<String, Object>> getCheckPrintHeadList (HashMap<String, Object> paramMap);
	
	public List<HashMap<String, Object>> getCheckPrintItemList (HashMap<String, Object> paramMap);

	public Boolean updateCheckAssignList(HashMap<String, Object> dataMap);

	public Boolean updateCheckStatusList(HashMap<String, Object> dataMap);

}
