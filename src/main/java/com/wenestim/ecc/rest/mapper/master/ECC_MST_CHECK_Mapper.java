package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_CHECK_Mapper
{
	public List<HashMap<String, Object>> getCheckBookList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getAvailBookList(HashMap<String, Object> paramMap);

	public Boolean updateCheckBookList(HashMap<String, Object> dataMap);
	
	public Boolean deleteCheckBookList(HashMap<String, Object> dataMap);
}
