package com.wenestim.ecc.rest.mapper.fs;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_FS_Mapper
{
	public List<HashMap<String, Object>> getFinancialStatementList(HashMap<String, Object> paramMap);
	public List<HashMap<String, Object>> getConsolidationList(HashMap<String, Object> paramMap);
	public List<HashMap<String, Object>> getDashboardDataList(HashMap<String, Object> paramMap);

}
