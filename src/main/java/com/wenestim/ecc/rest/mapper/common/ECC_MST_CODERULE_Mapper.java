package com.wenestim.ecc.rest.mapper.common;

import java.util.HashMap;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface ECC_MST_CODERULE_Mapper
{
	public String getMasterCode(HashMap<String, Object> paramMap);
	
	@Select("SELECT FN_ECC_GET_DOC_NO(#{CLIENT_ID}, #{COMPANY_CODE},#{CATEGORY2}, #{CODE}, ${POST_YEAR})")
	public String getDocNo(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("CATEGORY2") String CATEGORY2, @Param("CODE") String CODE, @Param("POST_YEAR") Integer POST_YEAR);
}
