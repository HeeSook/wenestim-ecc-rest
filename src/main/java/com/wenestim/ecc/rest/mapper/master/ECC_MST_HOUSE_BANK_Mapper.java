package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_HOUSE_BANK_Mapper
{
	public List<HashMap<String, Object>> getBankHeaderList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getBankDetailList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getBankAccIdList (HashMap<String, Object> paramMap);

	public Boolean updateBankHeaderList(HashMap<String, Object> dataMap);

	public Boolean updateBankDetailList(HashMap<String, Object> dataMap);

	public Boolean deleteBankHeaderList(HashMap<String, Object> dataMap);

	public Boolean deleteBankDetailList(HashMap<String, Object> dataMap);

}
