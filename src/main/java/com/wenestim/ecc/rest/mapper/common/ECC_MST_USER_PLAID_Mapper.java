package com.wenestim.ecc.rest.mapper.common;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_USER_PLAID_Mapper 
{
	public List<HashMap<String, Object>> getUserAccToken(HashMap<String, Object> paramMap);
	
	public Boolean updateUserAccToken(HashMap<String, Object> dataMap);
}
