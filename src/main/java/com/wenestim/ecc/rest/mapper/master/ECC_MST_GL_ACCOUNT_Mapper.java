package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_GL_ACCOUNT_Mapper
{
	public List<HashMap<String, Object>> getCoaGlAccountList    (HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getCompanyGlAccountList(HashMap<String, Object> paramMap);

	public Boolean updateCoaGlAccountList    (HashMap<String, Object> dataMap);

	public Boolean createCompanyGlAccountList(HashMap<String, Object> dataMap);

	public Boolean updateCompanyGlAccountList(HashMap<String, Object> dataMap);

	public Boolean deleteCoaGlAccountList    (HashMap<String, Object> dataMap);

	public Boolean deleteCompanyGlAccountList(HashMap<String, Object> dataMap);

	public List<HashMap<String, Object>> getCoaExcelUploadCheckList(HashMap<String, Object> paramMap);
}
