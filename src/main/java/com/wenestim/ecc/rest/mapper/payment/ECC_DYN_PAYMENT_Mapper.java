package com.wenestim.ecc.rest.mapper.payment;

import java.util.HashMap;
import java.util.List;

public interface ECC_DYN_PAYMENT_Mapper
{
	public List<HashMap<String, Object>> getOpenItemList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getSimulationList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getPostingResultList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getPaymentList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getIsOpenItemList(HashMap<String, Object> paramMap);

	public Boolean postPaymentList(HashMap<String, Object> dataMap);

	public Boolean postInterPaymentList(HashMap<String, Object> dataMap);
}
