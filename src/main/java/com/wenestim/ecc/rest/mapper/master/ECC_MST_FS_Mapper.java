package com.wenestim.ecc.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Select;

public interface ECC_MST_FS_Mapper
{

	public List<HashMap<String, Object>> getFinancialStatementHierarchyList(HashMap<String, Object> paramMap);
	
	public List<HashMap<String, Object>> checkFSCodeExists(HashMap<String, Object> paramMap);
	
	public List<HashMap<String, Object>> getFSParentCodeList(HashMap<String, Object> paramMap);
	
	public List<HashMap<String, Object>> getFSChildCodeList(HashMap<String, Object> paramMap);
	
	public List<HashMap<String, Object>> existsFixName(HashMap<String, Object> paramMap);

	@Select("SELECT FN_ECC_CREATE_FS_VER(#{CLIENT_ID},#{COA_CODE},#{CODE_DESC})")
	public String  createFS(HashMap<String, Object> dataMap);
	
	public Boolean insertFSMaster(HashMap<String, Object> dataMap);

	public Boolean updateFSMaster(HashMap<String, Object> dataMap);
	
	public Boolean deleteFSMaster(HashMap<String, Object> dataMap);

}
