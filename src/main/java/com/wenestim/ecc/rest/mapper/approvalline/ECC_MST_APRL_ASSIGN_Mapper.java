package com.wenestim.ecc.rest.mapper.approvalline;

import java.util.HashMap;
import java.util.List;

public interface ECC_MST_APRL_ASSIGN_Mapper
{
	public List<HashMap<String, Object>> getAprlLineList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getAprlAssingList(HashMap<String, Object> paramMap);

	public Boolean updateAprlAssingList(HashMap<String, Object> dataMap);
}
