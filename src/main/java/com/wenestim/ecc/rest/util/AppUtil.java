package com.wenestim.ecc.rest.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * @author Andrew
 *
 */
public class AppUtil
{

	final static Logger logger = LogManager.getLogger(AppUtil.class);

	private static Properties properties = new Properties();
	private static InputStream input     = null;
	private static String PROPERTY_VALUE = "";
	private static String filename       = "wenestim.ecc.properties";

	public AppUtil( ) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param PROPERTY_NAME
	 * @return PROPERTY_VALUE
	 * @throws IOException
	 */
	public static String getPropertyByName(String PROPERTY_NAME) throws IOException
	{

		try
		{
			input = DBUtil.class.getClassLoader().getResourceAsStream(filename);

			if(input==null)
			{
				logger.warn("Error Occurred, Unable to find " + filename);
	            return null;
			}

			//load a properties from inside of property file
			properties.load(input);

            //get the property value
    		PROPERTY_VALUE = properties.getProperty(PROPERTY_NAME);

		}
		catch (IOException e)
		{
			logger.error("IOException: " + e.getMessage());
		}
		finally
		{
			if(input!=null)
			{
        		try
        		{
        			input.close();
        		}
        		catch (IOException e)
        		{
        			logger.error("IOException: " + e.getMessage());
        		}
        	}
		}

		return PROPERTY_VALUE;
	}
}
