package com.wenestim.ecc.rest.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.JsonObject;

public class StringUtil
{
	private static Logger logger = LogManager.getLogger(StringUtil.class);

	public StringUtil()
	{
	}

	public static String getEmptyNullPrevString(String CHK_STRING)
	{
		try
		{
			CHK_STRING = CHK_STRING == null || CHK_STRING.equals("") || CHK_STRING.equals("null") ? null : CHK_STRING;
		}
		catch (Exception e)
		{
			logger.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return CHK_STRING;
	}

	public static String getConversionDateFormat(String I_DATE)
	{
		String rtnDate = "";
		try
		{
			if (I_DATE != null)
			{
				rtnDate = I_DATE.substring(0, 10);
			}
		}
		catch (Exception e)
		{
			logger.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return rtnDate;
	}

	public static String getInputStreamToString(InputStream inputStream)
	{
		String rsltString = "";
		try
		{
			StringBuilder stringBuilder = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			String line;

			while((line = in.readLine()) != null)
			{
				stringBuilder.append(line);
			}
			rsltString = stringBuilder.toString();
//			logger.info("rsltString: "+rsltString);

		}
		catch (Exception e)
		{
			logger.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return rsltString;
	}

	public static HashMap getParamMap(String inputString)
	{
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		try
		{
			int    splitPos   = inputString.indexOf("^");
			if( splitPos == -1)
			{
				return paramMap;
			}
			String paramString = inputString.substring(0,splitPos);
			paramString = "[" + paramString.substring(1, paramString.length()) + "]";
			JSONArray paramList = new JSONArray(paramString);

			for(int i=0; i<paramList.length(); i++)
			{
				JSONObject jsonObject = paramList.getJSONObject(i);
				Iterator iterator = jsonObject.keys();
				while (iterator.hasNext())
				{
				    String key   = iterator.next().toString();
					String value = jsonObject.has(key) ? jsonObject.getString(key) : "";
				    paramMap.put(key, value);
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return paramMap;
	}

	public static HashMap<String,Object> getParamMap(InputStream inputStream)
	{
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
	        if( getString == null || getString.equals(""))
	        {
	    		return paramMap;
	        }

			JSONObject jsonObject = (new JSONArray(getString)).getJSONObject(0);
			Iterator iterator = jsonObject.keys();
			while (iterator.hasNext())
			{
			    String key   = iterator.next().toString();
				String value = jsonObject.has(key) ? jsonObject.getString(key) : "";
				if( value == null || value.equals("") )
				{
					value = null;
				}
			    paramMap.put(key, value);
			    logger.info(key + ":"+value);
			}
		}
		catch (Exception e)
		{
			logger.error("Exception: "+e.getMessage());
			e.printStackTrace();
			paramMap.put("errorMsg", e.getMessage());
		}
		return paramMap;
	}

	public static ArrayList<HashMap<String,Object>> getJsonToHashMapList(String paramString)
	{
		ArrayList<HashMap<String,Object>> paramList = new ArrayList<HashMap<String,Object>>();

		try
		{
			if(paramString == null || paramString.equals(""))
			{
				return paramList;
			}

			JSONArray jsArray = new JSONArray(paramString);
			for (int i=0; i<jsArray.length(); i++)
			{

				HashMap<String,Object> paramMap = new HashMap<String,Object>();
				JSONObject jsonObject = jsArray.getJSONObject(i);
				Iterator iterator = jsonObject.keys();
				while (iterator.hasNext())
				{
				    String key   = iterator.next().toString();
				    String value = jsonObject.has(key) ? jsonObject.getString(key) : "";
				    if( value != null && value.equals("") )
					{
						value = null;
					}
				    paramMap.put(key, value);
				    logger.info(key + ":"+value);

				}
				paramList.add(paramMap);
			}
		}
		catch (Exception e)
		{
			logger.error("Exception: "+e.getMessage());
			e.printStackTrace();
			HashMap<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put("errorMsg", e.getMessage());
			paramList.add(paramMap);
		}
		return paramList;
	}

	public static HashMap<String,Object> copyHashMap(HashMap<String,Object> srcMap, HashMap<String,Object> tarMap)
	{
		try
		{
			for ( String key : srcMap.keySet() )
			{
				String value = (String)srcMap.get(key);
				tarMap.put(key, value);
				logger.info(key + ":"+value);
			}
		}
		catch (Exception e)
		{
			logger.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return tarMap;
	}

	public static ArrayList<Object> getJsonToObjectList(String paramString)
	{
		ArrayList<Object> paramList = new ArrayList<Object>();

		try
		{
			JSONArray jsArray = new JSONArray(paramString);
			for (int i=0; i<jsArray.length(); i++)
			{
				if( !jsArray.get(i).equals(""))
				{
					paramList.add(jsArray.get(i));
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return paramList;
	}

	public static String getRegExpIsPlusNumber()
	{
		return "^[0-9]*[.]?[0-9|.]+$";
	}

	public static String getRegExpIsAllNumber()
	{
		return "^-?[0-9]*[.]?[0-9|.]+$";
	}

	public static String getIntTransNo(String companyCode, String docNo, String postYear)
	{
		return companyCode + "_" + docNo + "_" + postYear;
	}

	public static JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
	{
		JsonObject rtnJson = new JsonObject();
		if (rtnBoolResult.booleanValue())
		{
			rtnJson.addProperty("ret", Integer.valueOf(0));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", "confirm");
		}
		else
		{
			String rtnMsg = "could not load data";
			rtnJson.addProperty("ret", Integer.valueOf(1));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", rtnMsg);
		}
		return rtnJson;
	}


	/**
	 * Convert to ArrayList with HashMap Included Null Field
	 * @param resultList
	 * @param string
	 * @return
	 */
	public static List<HashMap<String, Object>> getIncludeNullField(List<HashMap<String, Object>> resultList,String string)
	{
		// TODO Auto-generated method stub
		try
		{
			for (HashMap<String,Object> map : resultList)
        	{
        		Object IncludeNullField = map.get(string);

        		if(IncludeNullField == null)
        		{
        			map.put(string, null);
        		}
        	}
		}
		catch(Exception e)
		{
			logger.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return resultList;
	}

}
