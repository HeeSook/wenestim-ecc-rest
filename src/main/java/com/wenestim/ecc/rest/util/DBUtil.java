package com.wenestim.ecc.rest.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DBUtil
{
	final static Logger logger = LogManager.getLogger(DBUtil.class);

	private final static String PROPERTY = "wenestim.ecc.properties";

	public DBUtil()
	{

	}

	public static Connection getOraDBConnection() throws IOException, ClassNotFoundException, SQLException
	{
		Properties properties   = new Properties();
		Connection dbConnection = null;

		try
		{
			InputStream input =	DBUtil.class.getClassLoader().getResourceAsStream(PROPERTY);

			if(input == null)
			{
				throw new Exception("Unable to find ["+PROPERTY+"]");
			}

			properties.load(input);

    		String ORA_DB_DRIVER     = properties.getProperty("ORA_DB_DRIVER");
    		String ORA_DB_CONNECTION = properties.getProperty("ORA_DB_CONNECTION");
    		String ORA_DB_USER       = properties.getProperty("ORA_DB_USER");
    		String ORA_DB_PASS       = properties.getProperty("ORA_DB_PASS");

			Class.forName(ORA_DB_DRIVER);
			dbConnection = DriverManager.getConnection(ORA_DB_CONNECTION, ORA_DB_USER, ORA_DB_PASS);

			return dbConnection;

		}
		catch (IOException e)
		{
			logger.error("IOException: " + e.getMessage());
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			logger.error("ClassNotFoundException: " + e.getMessage());
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			logger.error("SQLException: " + e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e)
		{
			logger.error("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		return dbConnection;
	}
}
