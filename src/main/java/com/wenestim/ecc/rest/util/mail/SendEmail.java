package com.wenestim.ecc.rest.util.mail;

import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.codehaus.jettison.json.JSONObject;

/**
 * @author Andrew
 *
 */
public class SendEmail
{
	//@Resource(name = "velocityEngine")
	//private static VelocityEngine velocityEngine;

	private static Logger logger = LogManager.getLogger(SendEmail.class);

	private static String SMTP_SERVER = "smtp.office365.com";
	private static String SMTP_PORT   = "587";

	private static String AUTH_USER   = "info@wenestim.com";
	private static String AUTH_PASS   = "Atns1234!";

	private static String EMAIL_FROM  = "info@wenestim.com";
	private static String EMAIL_TO    = "info@wenestim.com";

	public static Boolean sendEmail(JSONObject jsObj, String strForm)
	{
		strForm = strForm.toLowerCase();

		Path   currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		logger.info("Dir: "+System.getProperty("user.dir"));
		logger.info("Path: "+s);
		Boolean pass = false;
		if(strForm.toLowerCase() == "default")
		{
			return sendingEmail(jsObj);
		}
		else
		{
			if( strForm.equals("default"))
			{
				pass = sendingEmail(jsObj);
			}
			else if( strForm.equals("reject") || strForm.equals("approve"))
			{

			}
			else
			{
				pass = sendEmailForm(jsObj, strForm);
			}
		}
		return pass;
	}

	public static Boolean sendingEmail(JSONObject jsObj)
	{
		Boolean rslt = true;
		try {
			String name    = jsObj.has("name")    ? jsObj.getString("name")    : "";
			String company = jsObj.has("company") ? jsObj.getString("company") : "";
			String email   = jsObj.has("email")   ? jsObj.getString("email")   : "";
			String phone   = jsObj.has("phone")   ? jsObj.getString("phone")   : "";
			String zip     = jsObj.has("zip")     ? jsObj.getString("zip")     : "";

			String subject = "[INFO REQUEST] ";
			subject = jsObj.has("subject") ? subject + jsObj.getString("subject") : subject;

			String msg_header = "INFORMATION REQUESTED BY HOMEPAGE AS BELOW, \n";
			msg_header += "\n NAME: "    + name;
			msg_header += "\n COMPANY: " + company;
			msg_header += "\n EMAIL: "   + email;
			msg_header += "\n PHONE: "   + phone;
			msg_header += "\n ZIP: "     + zip;
			msg_header += "\n\n";

			String content = jsObj.has("content") ? jsObj.getString("content") : "";

			Properties props = new Properties();
			props.put("mail.debug"               , "false");
			props.put("mail.transport.protocol"  , "smtp" );
			props.put("mail.smtp.auth"           , "true" );
			props.put("mail.smtp.starttls.enable", "true" );
			props.put("mail.smtp.host"           , SMTP_SERVER);
			props.put("mail.smtp.port"           , SMTP_PORT  );

			props.setProperty("mail.smtp.ssl.protocols", "TLSv1.1 TLSv1.2");

			Session mailSession = Session.getInstance(props, new Authenticator() {
				@Override
					public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(AUTH_USER, AUTH_PASS);
				}
			});
			mailSession.setDebug(false);

			Message message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress( EMAIL_FROM ));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse( EMAIL_TO ));

			message.setSubject(subject);
			message.setText(msg_header + content);

			Transport.send(message);
			logger.info("to : "+EMAIL_TO+", Subject : "+subject+" E-Mail Send Success.");

			return rslt;
		}
		catch (Exception e)
		{
			logger.error("Exception:"+e.getMessage());
			e.printStackTrace();

			return false;
		}
	}

	public static Boolean sendEmailForm(JSONObject jsObj, String form)
	{
		Boolean rslt = true;
		logger.info("jsObj for Email: "+jsObj.toString());

		VelocityContext context = new VelocityContext();
		VelocityEngine velocityEngine = new VelocityEngine();
		velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");//RuntimeConstants ==> Is the absolute path to the runtime existance of the code
		velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());//is the relative path to the resource from absolute path
		context.put(form, jsObj);

		StringWriter stringWriter = new StringWriter();
		String formVM = "templates/"+form+".vm";

		logger.info("formVM   : "+formVM);
		logger.info("context  : "+context.toString());
		logger.info("classpath: "+ClasspathResourceLoader.class.getName());

		velocityEngine.mergeTemplate(formVM, "UTF-8", context, stringWriter);
		//LOGGER.info("text: "+stringWriter.toString());

		String text = stringWriter.toString();

		try
		{
			String subject = jsObj.getString("EMAIL_TITLE");
			logger.info("Subject: "+subject);

			Properties props = new Properties();
			props.put("mail.debug"               , "false");
			props.put("mail.transport.protocol"  , "smtp" );
			props.put("mail.smtp.auth"           , "true" );
			props.put("mail.smtp.starttls.enable", "true" );
			props.put("mail.smtp.host"           , SMTP_SERVER);
			props.put("mail.smtp.port"           , SMTP_PORT  );

			props.setProperty("mail.smtp.ssl.protocols", "TLSv1.1 TLSv1.2");

			Session mailSession = Session.getInstance(props, new Authenticator() {
				@Override
					public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(AUTH_USER, AUTH_PASS);
				}
			});
			mailSession.setDebug(false);

			Message message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress( EMAIL_FROM ));
	 	  //message.setRecipients(Message.RecipientType.TO, InternetAddress.parse( EMAIL_TO ));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse( jsObj.getString("RECEIVER_EMAIL") ));

			message.setSubject(subject);
			//message.setText();
			message.setContent(text, "text/html; charset=UTF-8");

			Transport.send(message);
			logger.info("to : "+jsObj.getString("RECEIVER_EMAIL")+", Subject : "+subject+" E-Mail Send Success.");

			return rslt;
		}
		catch (Exception e)
		{
			logger.error("Exception:"+e.getMessage());
			e.printStackTrace();

			return false;
		}
	}

    public static Boolean sendEmailForm(HashMap<String,Object> mailItem, String form)
    {
        Boolean rslt = true;
        try
        {
        	logger.info("mailObj for mailItem: "+mailItem.toString());
            String formVM  = "templates/" + form+".vm";
            String subject = (String)mailItem.get("EMAIL_TITLE");
            String toMail  = (String)mailItem.get("EMAIL_TO"   );

            if(!validMailItem("Subject", subject) || !validMailItem("Mail To", toMail))
            {
            	return false;
            }

            logger.info("formVM :: " + formVM);

            VelocityEngine  velocityEngine  = new VelocityEngine() ;
            VelocityContext velocityContext = new VelocityContext();
            StringWriter    stringWriter    = new StringWriter()   ;

            velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");//RuntimeConstants ==> Is the absolute path to the runtime existance of the code
            velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());//is the relative path to the resource from absolute path


            for(String key: mailItem.keySet())
            {
            	velocityContext.put(key, mailItem.get(key));
            }

            velocityEngine.mergeTemplate(formVM, "UTF-8", velocityContext, stringWriter);
            String content = stringWriter.toString();
            Properties props = new Properties();

            //2. TLS 1.2
            // Set debug so we see the whole communication with the server
            props.put("mail.debug"             , "false"    );
            props.put("mail.transport.protocol", "smtp"     );
            props.put("mail.host"              , SMTP_SERVER);
            props.put("mail.smtp.auth"         , "true"     );
            props.put("mail.smtp.port"         , SMTP_PORT  );

            // Enable STARTTLS
            props.put("mail.smtp.starttls.enable", "true");
            // Accept only TLS 1.1 and 1.2
            props.setProperty("mail.smtp.ssl.protocols", "TLSv1.1 TLSv1.2");

            Session mailSession = Session.getInstance(props, null);
            mailSession.setDebug(false);

            // Create an SMTP transport from the session
            Transport t = mailSession.getTransport("smtp");

            // Connect to the server using credentials
            t.connect(SMTP_SERVER, AUTH_USER, AUTH_PASS);

            // Make message
            Message message = new MimeMessage(mailSession);
            message.setFrom(new InternetAddress( EMAIL_FROM ));
//            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse( toMail ));
            message.setRecipients(Message.RecipientType.TO, getRecipients(toMail));
            message.setSubject(subject);
            message.setContent(content, "text/html; charset=UTF-8");
            // Send the message
            t.sendMessage(message, message.getAllRecipients());

            logger.info("content : "+content);
            logger.info("to : "+toMail+", Subject : "+subject+" E-Mail Send Success.");
            return rslt;
        }
        catch (Exception e)
        {
            logger.error("Exception:"+e.getMessage());
            e.printStackTrace();

            return false;
        }
    }

    private static Address[] getRecipients(String eMail) throws AddressException
    {
        String[] mailList = eMail.split(",");
        ArrayList<String> tempList = new ArrayList<String>();

        for(String mailItem : mailList)
        {
        	if( mailItem == "" || mailItem.equals("") || tempList.contains(mailItem))
        	{
        		continue;
        	}
        	tempList.add(mailItem);
        }

        Address[] addressList = new Address[tempList.size()];
        for (int i = 0;i < tempList.size();i++)
        {
        	addressList[i] = new InternetAddress(mailList[i]);
        }
        logger.info("addressList-To::"+tempList.toString());
        return addressList;
    }

    private static Boolean validMailItem(String key, String value)
    {
    	logger.info(key + " :: " + value);
    	if( value == null || value.equals("") )
        {
        	try
        	{
        		throw new Exception(key + " is null");
        	}
        	catch (Exception e)
        	{
        		logger.error("Exception:"+e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
    	return true;
    }

}
