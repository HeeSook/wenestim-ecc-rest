package com.wenestim.ecc.rest.util;

import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyBatisUtil
{

	private static Logger logger = LogManager.getLogger(MyBatisUtil.class);
	private static SqlSessionFactory factory;

	public MyBatisUtil()
	{

	}

	static
	{
		Reader reader = null;

		try
		{
			reader = Resources.getResourceAsReader("wenestim-ecc-config.xml");
			factory = new SqlSessionFactoryBuilder().build(reader);
		}
		catch (IOException e)
		{
			logger.error("MyBatisUtil() IOException:"+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	public static SqlSessionFactory getSqlSessionFactory()
	{
		return factory;
	}

	public static SqlSessionFactory getOraSessionFactory()
	{
		SqlSessionFactory oraFactory;
		Properties properties = new Properties();
		Reader reader = null;

		try
		{
			reader = Resources.getResourceAsReader("wenestim-ecc-config.xml");
			oraFactory = new SqlSessionFactoryBuilder().build(reader,"oracle");
		}
		catch (IOException e)
		{
			logger.error("MyBatisUtil() IOException:"+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return oraFactory;
	}
}
